const request = require('supertest');
const { faker } = require('@faker-js/faker');
const app = require('../src/app');

const accountService = require('../src/services/accounts.service');
const vendorService = require('../src/services/vendors.service');
const TOKEN = require('./fixture/token.fixture');
const vendorFixture = require('./fixture/vendor.fixture');

const columns = ['id', 'AccountId', 'isVerified', 'categories', 'name', 'information', 'address', 'city', 'phone', 'document'];
const columnRelation = [
  'id',
  'AccountId',
  'isVerified',
  'categories',
  'name',
  'information',
  'address',
  'city',
  'phone',
  'document',
  'Account.email',
  'Account.roles',
];

describe('Vendor routes', () => {
  test('[Authorization Check] should return 401 if token is invalid', async () => {
    const res = await request(app)
      // Get all vendors endpoint
      .get('/api/v1/vendors')
      // Send request
      .send()
      // Expect status code
      .expect(403);

    // Expect response body equeals to { code, data }
    expect(res.body).toEqual({
      code: 403,
      error: {},
      message: 'Authorization failed',
    });
  });

  describe('GET /api/v1/vendors', () => {
    test('should return 200 and show all vendors data', async () => {
      const res = await request(app)
        // Get all vendors endpoint
        .get('/api/v1/vendors')
        // Set cookies
        .set('Cookie', ['_acct=' + TOKEN.VENDOR])
        // Send request
        .send()
        // Expect status code
        .expect(200);

      // Expect response body equeals to { code, data }
      expect(res.body).toEqual({
        code: 200,
        data: expect.any(Array),
      });
      // Expect array of object properties
      expect(Object.keys(res.body.data[0])).toEqual(columns);
    });
  });

  describe('GET /api/v1/vendors/r/accounts', () => {
    test('should return 200 and show all vendors data relationship with accounts', async () => {
      const res = await request(app)
        // Get all vendors endpoint relationship with accounts
        .get('/api/v1/vendors/r/accounts')
        // Set cookies
        .set('Cookie', ['_acct=' + TOKEN.VENDOR])
        // Send request
        .send()
        // Expect status code
        .expect(200);

      // Expect response body equeals to { code, data }
      expect(res.body).toEqual({
        code: 200,
        data: expect.any(Array),
      });
      // Expect array of object properties
      expect(Object.keys(res.body.data[0])).toEqual(columnRelation);
    });
  });

  describe('GET /api/v1/vendors/:id', () => {
    test('should return 200 and show one vendor data', async () => {
      const res = await request(app)
        // Get one vendors endpoint
        .get('/api/v1/vendors/b12b63fe-7850-4279-9497-8e2dff50e9dd')
        // Set cookies
        .set('Cookie', ['_acct=' + TOKEN.VENDOR])
        // Send request
        .send()
        // Expect status code
        .expect(200);

      // Expect response body equeals to { code, data }
      expect(res.body).toEqual({
        code: 200,
        data: expect.any(Object),
      });
      // Expect array of object properties
      expect(Object.keys(res.body.data)).toEqual(columns);
    });
  });

  describe('GET /api/v1/vendors/r/accounts/:id', () => {
    test('should return 200 and show one vendor data relationship with account', async () => {
      const res = await request(app)
        // Get all vendors endpoint relationship with accounts
        .get('/api/v1/vendors/r/accounts/b12b63fe-7850-4279-9497-8e2dff50e9dd')
        // Set cookies
        .set('Cookie', ['_acct=' + TOKEN.VENDOR])
        // Send request
        .send()
        // Expect status code
        .expect(200);

      // Expect response body equeals to { code, data }
      expect(res.body).toEqual({
        code: 200,
        data: expect.any(Object),
      });
      // Expect array of object properties
      expect(Object.keys(res.body.data)).toEqual(columnRelation);
    });
  });

  describe('POST /api/v1/auth/register?role=vendor', () => {
    test('should return 200 and vendor registered', async () => {
      const data = await vendorFixture.vendorTwo();
      const res = await request(app)
        // Register vendor endpoint
        .post('/api/v1/auth/register?role=vendor')
        // Send request
        .send(data)
        // Expect status code
        .expect(200);
      // Expect response body equeals to { code, data }
      expect(res.body).toEqual({
        code: 200,
        data: expect.any(Object),
      });
      // Expect data object properties
      expect(Object.keys(res.body.data)).toEqual([
        'id',
        'avatar',
        'roles',
        'email',
        'password',
        'isVerified',
        'categories',
        'name',
        'information',
        'address',
        'city',
        'phone',
        'AccountId',
        'document',
      ]);
      // Remove Vendor after update
      const removeVendor = await vendorService.remove(res.body.data.id);
      expect(removeVendor).toEqual(1);
      // Remove Account after update
      const removeAccount = await accountService.remove(res.body.data.AccountId);
      expect(removeAccount).toEqual(1);
    });

    test('should return 400 if vendor already exist', async () => {
      const res = await request(app)
        // Register vendor endpoint
        .post('/api/v1/auth/register?role=vendor')
        // Send request
        .send(vendorFixture.vendorOneExist)
        // Expect status code
        .expect(400);
      // Expect response body equeals to { code, data }
      expect(res.body).toEqual({
        code: 400,
        error: expect.any(Object),
        message: expect.any(String),
      });
    });
  });

  describe('PUT /api/v1/vendors/:id', () => {
    test('should return 200 and vendor updated', async () => {
      const data = await vendorFixture.vendorTwo();
      // Insert new vendor
      const createVendor = await accountService.createRegister('vendor', data);
      const vendorID = createVendor.id;
      const accountID = createVendor.AccountId;
      // Update vendor
      const res = await request(app)
        // Update vendor endpoint
        .put('/api/v1/vendors/' + vendorID)
        // Set cookies
        .set('Cookie', ['_acct=' + TOKEN.VENDOR])
        // Send request body, not all data included
        .send({
          information: faker.lorem.paragraph(),
          address: faker.address.streetAddress(true),
        })
        // Expect status code
        .expect(200);
      // Expect updated data
      expect(res.body).toEqual({
        code: 200,
        data: [1],
      });
      // Remove Vendor after update
      const removeVendor = await vendorService.remove(vendorID);
      expect(removeVendor).toEqual(1);
      // Remove Account after update
      const removeAccount = await accountService.remove(accountID);
      expect(removeAccount).toEqual(1);
    });
  });

  describe('DELETE /api/v1/vendors/:id', () => {
    test('should return 200 and vendor deleted', async () => {
      const data = await vendorFixture.vendorTwo();
      // Insert new vendor
      const createVendor = await accountService.createRegister('vendor', data);
      const vendorID = createVendor.id;
      const accountID = createVendor.AccountId;
      // Delete vendor
      const res = await request(app)
        // Delete vendor endpoint
        .delete('/api/v1/vendors/' + vendorID)
        // Set cookies
        .set('Cookie', ['_acct=' + TOKEN.VENDOR])
        // Send request body, not all data included
        .send()
        // Expect status code
        .expect(200);
      // Expect deleted data
      expect(res.body).toEqual({
        code: 200,
        data: 1,
      });
      // Remove Account after delete Vendor
      const removeAccount = await accountService.remove(accountID);
      expect(removeAccount).toEqual(1);
    });
  });
});
