const request = require('supertest');
const app = require('../src/app');
const jwt = require('../src/utils/jwt');

const credentials = {
  email: 'vendor@gmail.com',
  password: '123456',
};
/**
 * Auth Routes Test
 */
describe('Auth routes', () => {
  /**
   * Login as Vendor
   */
  describe('Login as Vendor', () => {
    test('should return 200 and cookies has been set', async () => {
      const res = await request(app)
        // Request to login
        .post('/api/v1/auth/login')
        // Request body
        .send(credentials)
        // Status code
        .expect(200)
        // Expect cookies
        .expect('set-cookie', /_acct=.*; Path=\/; HttpOnly/);
      // Expect response body to have token
      expect(res.body.data).toHaveProperty('token');
      // Expect token to be valid
      expect(jwt.verify(res.body.data.token)).toEqual(expect.any(Object));
    });
    /**
     * Test to check if email not found
     */
    test('should return 401 if email not found', async () => {
      const res = await request(app)
        .post('/api/v1/auth/login')
        // Custom email to be failed
        .send({
          ...credentials,
          email: credentials.email + 'willfailed',
        })
        // Expect unauthorized
        .expect(401);

      expect(res.body).toEqual({
        code: 401,
        error: {},
        message: 'Account not found',
      });
    });
    /**
     * Test case for wrong password
     */
    test('should return 401 if password is false', async () => {
      const res = await request(app)
        .post('/api/v1/auth/login')
        // Custom email to be failed
        .send({
          ...credentials,
          password: credentials.password + 'willfailed',
        })
        // Expect unauthorized
        .expect(401);

      expect(res.body).toEqual({
        code: 401,
        error: {},
        message: 'Email or password incorrect',
      });
    });
  });
});
