const { faker } = require('@faker-js/faker');
const bcrypt = require('../../src/utils/bcrypt');
const VendorService = require('../../src/services/vendors.service');

const vendorOneExist = {
  id: 'b12b63fe-7850-4279-9497-8e2dff50e9dd',
  AccountId: 'f622f488-f748-4a2f-a9a6-1b4df0f2ca6a',
  categories: ['Restaurant', 'Cafe'],
  name: 'Vendor abcd',
  information: 'Vero justo sanctus at vero amet at. Dolor sed sit rebum lorem, et ut.',
  address: 'Jl. Kelinci',
  city: 'Bekasi',
  phone: '08977787',
};

const vendorTwo = async () => {
  return {
    email: faker.internet.email(),
    password: await bcrypt.hash(faker.internet.password(20)),
    categories: faker.random.arrayElements(VendorService.categories),
    name: faker.name.findName(),
    information: faker.lorem.paragraph(),
    address: faker.address.streetAddress(true),
    city: faker.address.city(),
    phone: faker.phone.phoneNumber('08###########'),
  };
};

module.exports = {
  vendorOneExist,
  vendorTwo,
};
