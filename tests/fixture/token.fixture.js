const jwt = require('../../src/utils/jwt');

const CLIENT = jwt.sign({
  id: '048f9855-1ec7-4bdb-94ac-f7af68494dba',
  roles: 'CLIENT',
  email: 'client@gmail.com',
  name: 'Alvin Naufal',
  phone: '0812345678',
});

const VENDOR = jwt.sign({
  id: 'f622f488-f748-4a2f-a9a6-1b4df0f2ca6a',
  roles: 'VENDOR',
  email: 'vendor@gmail.com',
  name: 'Vendor abcd',
  phone: '08977787',
});

module.exports = { CLIENT, VENDOR };
