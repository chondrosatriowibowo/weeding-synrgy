addEventListener('load', () => {
  console.log('DOM fully loaded and parsed');
});

const loadSwiperDetailProduct = () => {
  const galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesProgress: true,
  });
  const galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    thumbs: {
      swiper: galleryThumbs,
    },
  });
};

const loadSwiperHome = () => {
  const swiper = new Swiper('.home-swiper', {
    slidesPerView: 1,
    spaceBetween: 30,
    // grabCursor: true,
    centeredSlides: true,
    initialSlide: 1,
    pagination: {
      el: '.swiper-pagination',
      clickable: 'yes',
    },
    breakpoints: {
      // when window width is >= 480px
      480: {
        slidesPerView: 'auto',
        spaceBetween: 30,
      },
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
};

//detailvendor ..............................
const rupiah = (number) => {
  const numb = number;
  const format = numb.toString().split('').reverse().join('');
  const convert = format.match(/\d{1,3}/g);
  return (number = convert.join('.').split('').reverse().join(''));
};

// product ============================
function getURL() {
  let a = window.location.href;
  if (a == 'https://promised.herokuapp.com:3000/vendors') {
    const api_url = 'https://promised.herokuapp.com:3000/api/v1/vendors';
    async function getapi(url) {
      const response = await fetch(url);
      var data = await response.json();
      if (response) {
        hideloader();
      }
      show(data);
    }
    getapi(api_url);

    function hideloader() {
      // document.getElementById('myItems').style.display = 'none';
    }

    function show(data) {
      db_vendor = data.data.rows;
      let cards = '';

      for (let n in db_vendor) {
        let str = db_vendor[n].starOutOf;
        let start = str.substring(0, 3);
        card = `<div class="card cardcust" onclick="window.location.href='/vendors/${db_vendor[n].id}'">
    <img src="${db_vendor[n].banner}" class="card-image cardcust-image">
    </img>
    <div class="title">
        <h1 class="card-title cardcust-title">${
          db_vendor[n].name
        } <img class="verified" src="/images/verified-icon.png" alt="Verified" width="20" height="20"></h1>
    </div>
    <div class="detail">
        <p>${db_vendor[n].categories[0]}</p>
        <p>IDR <span id="rupiah">${rupiah(db_vendor[n].minPrice)} - IDR ${rupiah(db_vendor[n].maxPrice)}</span></p>
        <div class="review">
            <img src="/images/star.png" width="15" height="15">
            <p>${start}/5 (${db_vendor[n].totalReview} review)</p>
        </div>
        <div class="city">
            <img src="/images/location-icon.png" width="13" height="15">
            <p>${db_vendor[n].city}</p>
        </div>
    </div>
    </div>`;
        cards += card;
      }
      document.querySelector('.cards').innerHTML = cards;
    }

    var input = document.getElementById('myFilter');
    input.addEventListener('keyup', function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById('myBtn').click();
      }
    });
  } else if (a == 'https://promised.herokuapp.com:3000/products') {
    const api_url_product = 'https://promised.herokuapp.com:3000/api/v1/product';
    async function getapi(url) {
      const response = await fetch(url);
      var data = await response.json();
      if (response) {
        hideloader();
      }
      showdata(data);
    }
    getapi(api_url_product);

    function hideloader() {
      // document.getElementById('myItems').style.display = 'none';
    }

    function showdata(data) {
      db_product = data.data.rows;
      let cardsproduct = '';
      for (let n in db_product) {
        cardproduct = `<div class="cardproduct" onclick="window.location.href='/products/${db_product[n].id}'">
    <img src="${db_product[n].images}" class="card-image cardcust-image">
    </img>
    <div class="title">
        <h1 class="product-title">${db_product[n].name}</h1>
    </div>
    <div class="detail">
        <p>${db_product[n].Vendor.name}</p>
        <p>IDR <span id="rupiah">${rupiah(db_product[n].price)}</span></p>
        <div class="city">
            <img src="/images/location-icon.png" width="13" height="15">
            <p>${db_product[n].Vendor.city}</p>
        </div>
    </div>
    </div>`;
        cardsproduct += cardproduct;
        // console.log(db_vendor[n].name, 'dataaaaaaaa');
      }
      document.querySelector('.cardsproduct').innerHTML = cardsproduct;
    }
    var input = document.getElementById('productFilter');
    input.addEventListener('keyup', function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById('productBtn').click();
      }
    });
  } else {
    console.log('gagal');
  }
}
getURL();

var expanded,
  expanded1,
  expanded2,
  expanded3 = false;

function showKategori() {
  var checkboxes = document.getElementById('checkboxes-kategori');
  if (!expanded) {
    checkboxes.style.display = 'block';
    expanded = true;
  } else {
    checkboxes.style.display = 'none';
    expanded = false;
  }
}

function showHarga() {
  var checkboxes = document.getElementById('checkboxes-harga');
  if (!expanded1) {
    checkboxes.style.display = 'block';
    expanded1 = true;
  } else {
    checkboxes.style.display = 'none';
    expanded1 = false;
  }
}

function showLokasi() {
  var checkboxes = document.getElementById('checkboxes-lokasi');
  if (!expanded2) {
    checkboxes.style.display = 'block';
    expanded2 = true;
  } else {
    checkboxes.style.display = 'none';
    expanded2 = false;
  }
}

function showJenisVendor() {
  var checkboxes = document.getElementById('checkboxes-jenis-vendor');
  if (!expanded3) {
    checkboxes.style.display = 'block';
    expanded3 = true;
  } else {
    checkboxes.style.display = 'none';
    expanded3 = false;
  }
}

function searching() {
  var input, filter, myItems, cards, i, current, h1, text;
  input = document.getElementById('myFilter');
  filter = input.value.toUpperCase();
  myItems = document.getElementById('myItems');
  cards = myItems.getElementsByClassName('card');
  for (i = 0; i < cards.length; i++) {
    current = cards[i];
    h1 = current.getElementsByClassName('card-title')[0];
    text = h1.innerText.toUpperCase();
    if (text.indexOf(filter) > -1) {
      current.style.display = '';
    } else {
      current.style.display = 'none';
      //data yang dicari tidak ada, 404 page notfound
    }
  }
  document.querySelector('.search-input').value = '';
  document.querySelector('.search-input').placeholder = 'Cari Vendor';
}

function searchproduct() {
  var input, filter, myItems, cards, i, current, h1, text;
  input = document.getElementById('productFilter');
  filter = input.value.toUpperCase();
  myItems = document.getElementById('productItems');
  cards = myItems.getElementsByClassName('cardproduct');
  for (i = 0; i < cards.length; i++) {
    current = cards[i];
    h1 = current.getElementsByClassName('product-title')[0];
    text = h1.innerText.toUpperCase();
    if (text.indexOf(filter) > -1) {
      current.style.display = '';
    } else {
      current.style.display = 'none';
      //data yang dicari tidak ada, 404 page notfound
    }
  }
  document.querySelector('.search-input').value = '';
  document.querySelector('.search-input').placeholder = 'Cari Vendor';
}
