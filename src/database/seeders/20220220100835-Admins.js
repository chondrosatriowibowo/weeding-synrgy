'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Admins',
      [
        {
          id: '3a4213be-4755-42a0-bcf5-dfe02634209b',
          AccountId: '72d9abe5-cbb1-48d2-bc65-ce5bb2cc5685',
          name: 'Tina Jaya',
        },
        {
          id: '7cbb49ed-a3e5-4607-a87b-9ff353408300',
          AccountId: '2a1dd4a6-3726-4391-8200-08bcf1ce9eea',
          name: 'Tika Novina',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Admins', null, {});
  },
};
