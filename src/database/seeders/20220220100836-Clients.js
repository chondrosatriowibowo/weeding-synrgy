'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Clients',
      [
        {
          id: 'd1481241-76f0-4551-be84-80fdfd98e225',
          AccountId: '7ef3b232-8e78-4f9d-9931-fdba9cd0d2fe',
          AccountProviderId: null,
          name: 'Chondro Satrio',
          phone: '0832567716817',
        },
        {
          id: '63ed381c-24fe-4fd4-bb62-0b93c310f402',
          AccountId: '8d88a1d9-1c9a-4906-a5b0-546f725d2e7d',
          AccountProviderId: null,
          name: 'Aisyah Fitri',
          phone: '0879051749399',
        },
        {
          id: 'd90eab4a-257d-4a33-86e3-dd0866dddd7b',
          AccountId: 'cf3559bd-c7fe-43ca-90e9-4ae1c219ee11',
          AccountProviderId: null,
          name: 'Rifda Sajida',
          phone: '0880924201623',
        },
        {
          id: '59c34fe7-e568-45cf-99ee-087318f014cb',
          AccountId: '5ec40c92-f42a-4364-9a87-45d69903df4b',
          AccountProviderId: null,
          name: 'Thomas Shelby',
          phone: '0860441060355',
        },
        {
          id: '7fde95f3-dfae-4acc-939e-0989f8d62339',
          AccountId: '2debee63-fec7-4eaa-a41f-736710edac25',
          AccountProviderId: null,
          name: 'Andira Yunita',
          phone: '0817579322707',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Clients', null, {});
  },
};
