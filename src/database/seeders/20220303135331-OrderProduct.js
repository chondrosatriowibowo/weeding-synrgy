'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'OrderProducts',
      [
        {
          OrderId: 'c8695d3b-3e1e-4bda-a550-bff8a4778ff8',
          ProductId: '2f2c67d5-7ef4-4168-b4b1-4ad78277729d',
          price: 95000000,
          discount: 10,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("12 Januari 2021").toISOString(),
        },
        {
          OrderId: 'c8695d3b-3e1e-4bda-a550-bff8a4778ff8',
          ProductId: '14fe7ba9-1602-446f-b8fe-aba71841820d',
          price: 100000000,
          discount: 20,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("12 Januari 2021").toISOString(),
        },
        {
          OrderId: 'e0756cc5-de57-4e3b-87e5-fa0c502e3b22',
          ProductId: 'dfddaf7f-5dcd-44f5-86db-a2ad77530090',
          price: 80000000,
          discount: 0,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("13 Februari 2021").toISOString(),
        },
        {
          OrderId: 'e0756cc5-de57-4e3b-87e5-fa0c502e3b22',
          ProductId: 'a72f430f-ec2b-4a07-ae87-1b8bdd36d53c',
          price: 48000000,
          discount: 20,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("13 Februari 2021").toISOString(),
        },
        {
          OrderId: 'c86bdd6e-b9f3-4710-9c6a-6dab20e800ed',
          ProductId: '25d7a0be-d0a7-4bed-88f3-00d5c9c54f32',
          price: 9000000,
          discount: 10,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("19 Januari 2021").toISOString(),
        },
        {
          OrderId: 'c86bdd6e-b9f3-4710-9c6a-6dab20e800ed',
          ProductId: '55ea2472-0ee2-43ff-bdbe-820c9b6cbd7f',
          price: 9600000,
          discount: 20,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("19 Januari 2021").toISOString(),
        },
        {
          OrderId: '83bdda0e-f5c2-4896-9ed5-2c39122a1276',
          ProductId: '94ae61e5-7ad0-4333-8cbc-8aba6b2f65d7',
          price: 63000000,
          discount: 10,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("11 Februari 2021").toISOString(),
        },
        {
          OrderId: '83bdda0e-f5c2-4896-9ed5-2c39122a1276',
          ProductId: '906367a6-fcc1-4c3f-a81e-6e129955763f',
          price: 153000000,
          discount: 15,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("11 Februari 2021").toISOString(),
        },
        {
          OrderId: 'f57310e2-1d48-4f9a-9f52-6b409f7c52a9',
          ProductId: '366565ad-5bea-408a-8584-42de35265fbe',
          price: 142500000,
          discount: 0,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("11 Maret 2021").toISOString(),
        },
        {
          OrderId: 'f57310e2-1d48-4f9a-9f52-6b409f7c52a9',
          ProductId: 'cecb6f95-baa8-4468-80eb-985558c59a1a',
          price: 90000000,
          discount: 5,
          progress: 'FINISH',
          quantity: 1,
          book_date: new Date("11 Maret 2021").toISOString(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('OrderProducts', null, {});
  },
};
