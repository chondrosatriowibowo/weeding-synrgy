'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Feedbacks',
      [
        {
          id: '65534e89-1f35-457f-a919-706b792c4b95',
          ClientId: 'd1481241-76f0-4551-be84-80fdfd98e225',
          ProductId: '2f2c67d5-7ef4-4168-b4b1-4ad78277729d',
          title: 'AOT its on my wedding',
          rating: 4,
          comment:
            'The AOT concept that you guys brought was wrap!!! me and my wife really had fun party. It was not perfect but It would be happiest memories.',
          feedbackDate: new Date("13 Januari 2021").toISOString(),
        },
        {
          id: '7f2bd923-5753-42aa-a957-7012c10c1ce2',
          ClientId: 'd1481241-76f0-4551-be84-80fdfd98e225',
          ProductId: '14fe7ba9-1602-446f-b8fe-aba71841820d',
          title: 'Disappointed',
          rating: 2,
          comment:
            "It's less attractive in the adoption of the pirate theme, and the design of the One Piece anime is not neat enough so it's not astonishing in the wedding",
          feedbackDate: new Date("13 Januari 2021").toISOString(),
        },
        {
          id: '7a578349-2204-435a-b3f9-1c3905f8408c',
          ClientId: '63ed381c-24fe-4fd4-bb62-0b93c310f402',
          ProductId: 'dfddaf7f-5dcd-44f5-86db-a2ad77530090',
          title: 'Amazing wedding at Bali',
          rating: 5,
          comment:
            'Thank you so much for services that you provided!!! you guys are rock!!!. My Family, friends and colleagues said that your vendor is amazing and it is amazing so really thankful. Will be recommended',
          feedbackDate: new Date("14 Februari 2021").toISOString(),
        },
        {
          id: '24200f0f-c522-4ea5-b32b-339b909bb1c1',
          ClientId: '63ed381c-24fe-4fd4-bb62-0b93c310f402',
          ProductId: 'a72f430f-ec2b-4a07-ae87-1b8bdd36d53c',
          title: 'Best Photography Ever',
          rating: 5,
          comment:
            "Summer Capture Our Wedding Moment Perfectly. Excellent service, great to work with, the best quality of pictures! Overall I am totally satisfied with Summer Photography and team, I just can't get over how amazing the photos turned out.\nThank You Summer Photography 💕💕",
          feedbackDate: new Date("14 Februari 2021").toISOString(),
        },
        {
          id: 'e7da2ac5-3af0-465f-a597-db01763540fa',
          ClientId: 'd90eab4a-257d-4a33-86e3-dd0866dddd7b',
          ProductId: '25d7a0be-d0a7-4bed-88f3-00d5c9c54f32',
          title: 'Finest Makeup Weeding',
          rating: 4,
          comment:
            'This is absolutely amazing, the result is not disappointed at all, the name of your product is really describing your service. Once again thank you gaes!!!',
          feedbackDate: new Date("20 Januari 2021").toISOString(),
        },
        {
          id: 'bf833324-5fc4-4b7f-8873-fbbf749de615',
          ClientId: 'd90eab4a-257d-4a33-86e3-dd0866dddd7b',
          ProductId: '55ea2472-0ee2-43ff-bdbe-820c9b6cbd7f',
          title: 'The Best Dress',
          rating: 4,
          comment:
            'The dress is also very beautiful with some unique details. The quality of the cosmetic products used is very good. Vendor was very punctual in preparing everything. Good job for this services.',
          feedbackDate: new Date("20 Januari 2021").toISOString(),
        },
        {
          id: '93625c15-20ce-4a8f-bc81-7bc242e8f7bc',
          ClientId: '59c34fe7-e568-45cf-99ee-087318f014cb',
          ProductId: '94ae61e5-7ad0-4333-8cbc-8aba6b2f65d7',
          title: 'Beyond Expectation',
          rating: 4,
          comment:
            'The decoration is beyond of my expectation thank you for the services that you provided. You guys are amazing and will be recommended for others who wants to have a wedding',
          feedbackDate: new Date("12 Februari 2021").toISOString(),
        },
        {
          id: '4ebc7466-2ac4-40b9-b99d-1ee6671f5ca2',
          ClientId: '59c34fe7-e568-45cf-99ee-087318f014cb',
          ProductId: '906367a6-fcc1-4c3f-a81e-6e129955763f',
          title: 'Not good',
          rating: 3,
          comment:
            'The decoration is not as expected, the vendor is not neat in working on the layout. Can be an improvement for the future. Hope you can improve your service',
          feedbackDate: new Date("12 Februari 2021").toISOString(),
        },
        {
          id: 'bd76f755-ca9e-4701-aa58-1a9464e56067',
          ClientId: '7fde95f3-dfae-4acc-939e-0989f8d62339',
          ProductId: '366565ad-5bea-408a-8584-42de35265fbe',
          title: 'Nice View',
          rating: 4,
          comment:
            'The areas provided are very interesting and have beautiful spots. The properties used are also of good quality and fit what I expected.',
          feedbackDate: new Date("12 Maret 2021").toISOString(),
        },
        {
          id: '6e47cd12-728e-4091-b1a1-7fa7789ddb22',
          ClientId: '7fde95f3-dfae-4acc-939e-0989f8d62339',
          ProductId: 'cecb6f95-baa8-4468-80eb-985558c59a1a',
          title: 'Its a WRAP setup',
          rating: 5,
          comment:
            'The food presentation and restaurant setup is very luxurious. The service served is also very good. The quality of the vendor exceeded my expectations, very good.',
          feedbackDate: new Date("12 Maret 2021").toISOString(),
        },
      ],
      {}
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Feedbacks', null, {});

  }
};
