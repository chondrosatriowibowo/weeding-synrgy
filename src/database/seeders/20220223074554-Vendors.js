'use strict';
const { randomString } = require('../../utils/generator');
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Vendors',
      [
        {
          id: '8fe348e9-d3b3-4adc-b204-5fc8b8f23814',
          AccountId: 'ff63edc8-b252-4e53-ac05-d0c2c585ea89',
          banner:
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/vendors%2FVendor%201.png?alt=media&token=6fae94cd-fcde-417b-8ec6-436ff0a716a8',
          isVerified: true,
          isBanned: false,
          categories: ['Photography'],
          name: 'Spring Photography',
          information:
            'Spring Photography adalah perusahaan fotografi pernikahan yang berbasis di Bali, Indonesia. Tujuan utama fotografer ini adalah memberi Anda koleksi gambar yang mencerminkan kisah otentik hari istimewa Anda.',
          experience:
            'Keterampilan Spring photography semi berkembang saat memotret keluarga besarnya di Indonesia dan momen-momen yang spesial bagi mereka. Spring photography semi terus mencari untuk meningkatkan keterampilannya dan belajar tentang tren dengan menghadiri lokakarya dan seminar fotografi di daerah tersebut.',
          service: 'Prewedding Photo, Wedding Photography',
          social_media: '{facebook: "", instagram: "https://www.instagram.com/dzakiizza/", twitter: ""}',
          address: 'Kabupaten Badung Kecamatan Kuta Utara Bali',
          city: 'Bali',
          phone: '085823006750',
          url: 'spring-so3sv',
          document: '',
        },
        {
          id: '4a221784-ea0c-4500-8911-d142ebfd0e28',
          AccountId: 'a851efa8-9062-4d19-934a-4ef5de02cdc3',
          banner:
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/vendors%2FVendor%202.png?alt=media&token=4c539ea0-65c5-4c25-bc6c-d9836916ab48',
          isVerified: true,
          isBanned: false,
          categories: ['Photography'],
          name: 'Anime Style Wedding',
          information:
            'Anime Style Wed adalah vendor Wedding dengan animasi ala Jepang. Ketika Anda memiliki imajinasi tentang anime, kita dapat melihat masa depan dengan keinginan Anda. Kami ingin mewujudkan keinginan Anda di dunia.',
          experience:
            'Kami memiliki produk bergaya anime ketika kami melihat klien yang lebih berpengaruh di masa depan mereka. Anime dengan Attack on titan, Anda dapat merasakan penyiangan dengan medan perang imajinasi Anda yang bergemuruh.',
          service:
            'Layanan kami meliputi Kostum Anime, Dekorasi Anime, Memori fotografi, make up gaya jepang, catering makanan dengan gaya jepang / anime dan lain-lain',
          social_media: '{facebook: "", instagram: "https://www.instagram.com/alvinnaufal/", twitter: ""}',
          address: 'Jl.MH Thamrin 51, DKI Jakarta',
          city: 'Jakarta',
          phone: '083083320035',
          url: 'anime-dkw3y',
          document: '',
        },
        {
          id: 'd7c7c90d-5b13-4c27-96e9-cb39db4bd680',
          AccountId: '6408449c-d221-4251-815c-312ea8790795',
          banner:
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/vendors%2FVendor%202.png?alt=media&token=4c539ea0-65c5-4c25-bc6c-d9836916ab48',
          isVerified: true,
          isBanned: false,
          categories: ['Venue', 'Cathering'],
          name: 'DeLuna Hotel Ball Room',
          information: 'DeLuna Hotel memberikan layanan ketring dan venue untuk berbagai kegiatan.',
          experience: '',
          service: '',
          social_media: '{facebook: "", instagram: "https://www.instagram.com/arasyyoram/", twitter: ""}',
          address: 'Jl.Karang Asem 8',
          city: 'Bandung',
          phone: '082019430251',
          url: 'deluna-kgalt',
          document: '',
        },
        {
          id: 'b1168d36-0be7-41ec-a618-3e6d1ced0d11',
          AccountId: '8f4b6c1a-c238-4c32-9ea0-e164a6b042dd',
          banner:
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/vendors%2FVendor%203.png?alt=media&token=68f7d546-5223-44a9-a66b-6f0096470431',
          isVerified: true,
          isBanned: false,
          categories: ['Dress & Attire', 'Makeup'],
          name: 'Makeupio dress',
          information:
            'Makeupio dress adalah praktisi kecantikan yang melayani pernikahan di wilayah metropolitan Surabaya.  Kami memberikan layanan make up dan dress sesuai tema pernikahan mu.',
          experience: '',
          service: 'Bridal, Wedding Make Up',
          social_media: '{facebook: "", instagram: "https://www.instagram.com/chondrowibowo/", twitter: ""}',
          address: 'Jl.Kyai Saleh',
          city: 'Surabaya',
          phone: '083082239658',
          url: 'makeupio-fflqg',
          document: '',
        },
        {
          id: '6db8deeb-22d5-44a5-9397-9f73af6ee5c3',
          AccountId: '87494dfd-3ba5-401b-83a5-70cc8cbd1bad',
          banner:
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/vendors%2FVendor%204.png?alt=media&token=37e792be-d30d-49eb-8631-b9f31f47474e',
          isVerified: true,
          isBanned: false,
          categories: ['Decoration'],
          name: 'Crystal Decor',
          information: 'Crystal Decor adalah spesialis dekorasi acara yang ada di Jakarta.',
          experience: '',
          service: 'Wedding Decoration, Engagement Decoration, Tradisional Decoration',
          social_media: '{facebook: "", instagram: "https://www.instagram.com/pppyna/", twitter: ""}',
          address: 'Jl.Mampang Prapatan Raya',
          city: 'Jakarta',
          phone: '082312274376',
          url: 'crystal-rvogb',
          document: '',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Vendors', null, {});
  },
};
