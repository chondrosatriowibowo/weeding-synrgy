'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Products',
      [
        {
          id: '2f2c67d5-7ef4-4168-b4b1-4ad78277729d',
          VendorId: '4a221784-ea0c-4500-8911-d142ebfd0e28',
          name: 'Rumbling on Marley',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%201a.png?alt=media&token=0700a775-04d3-435e-b3a0-7e5758606067',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%201b.png?alt=media&token=0170c965-9a25-4d25-9cf8-c342d4696e56',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%201c.png?alt=media&token=639b3cd4-a70d-4c33-b416-30815d91ebbe',
          ],
          description:
            'Costum: Titan Wedding + Corps Wedding\nDecoration: Marley Side / Paradise Side\nFood: Foot, Soup Meet 250x\nMakeUp: Japanese Style Wed',
          capacity: 250,
          discount: 10,
          price: 95000000,
          isActive: true,
        },
        {
          id: '14fe7ba9-1602-446f-b8fe-aba71841820d',
          VendorId: '4a221784-ea0c-4500-8911-d142ebfd0e28',
          name: 'Find One Piece',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%202a.jpg?alt=media&token=fd9ea437-ea11-48e1-a6d4-53408058f1dd',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%202b.png?alt=media&token=e79f6bff-c8f8-4a7c-9d28-cd964786cf8f',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%202c.jpg?alt=media&token=844e7d2c-9e8e-4369-a0e7-68d124e2f9b3',
          ],
          description:
            '-Costum: Pirates Luffy and Boa\n-Decoration: Sunny Go\n-Food: Beef, Fish Oden, 250x\n-MakeUp: Japenese Style Wed',
          capacity: 250,
          discount: 20,
          price: 125000000,
          isActive: true,
        },
        {
          id: 'dfddaf7f-5dcd-44f5-86db-a2ad77530090',
          VendorId: '8fe348e9-d3b3-4adc-b204-5fc8b8f23814',
          name: 'Intimate Wedding Package at Bali',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%203a.png?alt=media&token=fa565b62-be25-452d-8f00-a5b768d9a2ac',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%203b.png?alt=media&token=960cd82b-40a7-4d5d-8589-3fe390635d29',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%203c.png?alt=media&token=0fe86eb7-f36c-4328-8df5-b47c0a0d21fd',
          ],
          description:
            '- Exotic floral decoration including 6 (six) baskets of flowers petals for confetti \n- Flower petals along the walkway to the venue\n- Uniquely designed frangipani bride’s hand bouquet \n- Uniquely designed frangipani groom’s boutonniere\n- Priest Or Celebrant to lead the matrimony\n- Local Musicians playing Rindik; a traditional Balinese music instrument\n- Four (4) Balinese umbrella boys or 4 flower girls or a combination of both to escort the bride and groom to the altar\n- Two Romantic doves to be released  \n- Personalized wedding cake & 2 glass of champagne for toastin',
          capacity: 300,
          discount: 0,
          price: 80000000,
          isActive: true,
        },
        {
          id: 'a72f430f-ec2b-4a07-ae87-1b8bdd36d53c',
          VendorId: '8fe348e9-d3b3-4adc-b204-5fc8b8f23814',
          name: 'Summer Wedding Photography',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%204a.png?alt=media&token=1f628807-0c79-49cb-85ef-5acdde4cc393',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%204b.png?alt=media&token=c2c2956a-f98e-443e-be74-85efdf205c83',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%204c.png?alt=media&token=e1280ce7-59ee-4401-af79-14cd5654ad8a',
          ],
          description:
            '- Exotic island decoration including 6 (six) baskets of rare flowers petals for confetti \n- Flower petals along the walkway to the venue\n- Uniquely designed frangipani bride’s hand bouquet \n- Uniquely designed frangipani groom’s boutonniere\n- Priest Or Celebrant to lead the matrimony\n- Local Musicians playing Rindik; a traditional Balinese music instrument\n- Four (4) Balinese umbrella boys or 6 flower girls or a combination of both to escort the bride and groom to the altar\n- Two Romantic doves to be released  \n- Personalized wedding cake & 3 glass of champagne for toastin',
          capacity: 50,
          discount: 20,
          price: 60000000,
          isActive: true,
        },
        {
          id: '25d7a0be-d0a7-4bed-88f3-00d5c9c54f32',
          VendorId: 'b1168d36-0be7-41ec-a618-3e6d1ced0d11',
          name: 'Finest Makeup Weeding',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%205a.png?alt=media&token=d6b40553-8a67-4416-9ae6-6fc93ce651d2',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%205b.png?alt=media&token=72d0bda2-407c-43e8-a99d-0105a32f6c24',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%205c.png?alt=media&token=1709af20-5bd4-4e70-8f1e-85aaccc010fd',
          ],
          description:
            'Makeup & Hairdo no retouch Benefit: \n- Free Test Makeup (Makeup Only) \n- Free Rent Robe \n- Free Rent Crown (international look) \n- Free Softlens \n- Simple Makeup for Groom \n- Free transport (Jakarta Area) \n- All using high brand cosmetic',
          capacity: 5,
          discount: 10,
          price: 10000000,
          isActive: true,
        },
        {
          id: '55ea2472-0ee2-43ff-bdbe-820c9b6cbd7f',
          VendorId: 'b1168d36-0be7-41ec-a618-3e6d1ced0d11',
          name: 'The Best Dress',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%206a.png?alt=media&token=dc6d2566-c790-4e80-a902-a9d50861488a',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%206b.png?alt=media&token=68a1a9f2-b1eb-4f10-ad70-1a0b8934c019',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%206c.png?alt=media&token=36458e43-8663-409a-97df-b57962508cb2',
          ],
          description:
            'Makeup & Hairdo no retouch Benefit: \n- Free Test Dress (Dress Only) \n- Free Rent Robe \n- Free Rent Crown (international look) \n- Free Softlens \n- Simple Dress for Groom \n- Free transport (Jakarta Area) \n- All using high brand cosmetic',
          capacity: 5,
          discount: 20,
          price: 12000000,
          isActive: true,
        },
        {
          id: '94ae61e5-7ad0-4333-8cbc-8aba6b2f65d7',
          VendorId: '6db8deeb-22d5-44a5-9397-9f73af6ee5c3',
          name: 'Frozen Decoration Style',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%207a.png?alt=media&token=92bf883a-b392-4aa0-86f3-4cd9e7d06773',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%207b.png?alt=media&token=1d6936a4-b2ed-4141-a903-32606e06af55',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%207c.png?alt=media&token=772779a6-31b4-497d-8f46-9bb2f15df18c',
          ],
          description:
            '- Welcome Board\n- Snow\n- Deer Santa Claus\n- Mountain Snow background\n- Sweet Crystal Snow table decoration\n- Pohon Cemara\n- Beer brand',
          capacity: 200,
          discount: 10,
          price: 70000000,
          isActive: true,
        },
        {
          id: '906367a6-fcc1-4c3f-a81e-6e129955763f',
          VendorId: '6db8deeb-22d5-44a5-9397-9f73af6ee5c3',
          name: 'Chineese Decoration Style',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%208a.jpg?alt=media&token=02ed3c39-0ec6-4f3c-8d59-aeb5fb500469',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%208b.png?alt=media&token=aec2d8be-40c7-4bde-92ac-50f5ca8d77b2',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%208c.jpg?alt=media&token=dfe4b1b6-da45-4802-9672-517b6615c6c6',
          ],
          description:
            'CEREMONY DECORATION : \n- Welcome board \n- flower shower \n- Aisle decor \n- Altar Table \n- Centerpiece flower for alter table \n- Wedding arc standart \n\nDINNER DECORATION : \n- Registration area \n- Angpao Table \n- Photo gallery area \n- Gallery fotobooth \n- Guest table centerpiece (up to 10 table) \n- Bridal Table decorated 2 / 6 pax \n- Fairy light / bulbs',
          capacity: 100,
          discount: 15,
          price: 180000000,
          isActive: true,
        },
        {
          id: '366565ad-5bea-408a-8584-42de35265fbe',
          VendorId: 'd7c7c90d-5b13-4c27-96e9-cb39db4bd680',
          name: 'Glamore Dalune Smoothies',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%209a.jpg?alt=media&token=0be4d485-9747-46b3-96bf-744d4a9a2486',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%209b.png?alt=media&token=60f76443-ef6f-4561-b4de-b60f402048ea',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%209c.jpg?alt=media&token=4a2e2386-0df0-46f7-bee4-5cc3387ba5d1',
          ],
          description:
            'Stage Area:\n- Stage 12m (thematic design)\n- 5 pcs standing vase fresh flower for stage\n- 1 set sofa\n- Mini garden fresh flower\n- Stage carpet\nAisle Area:\n- 6 pcs standing vase fresh flower\n- 1 set gazebo 4x4m\n- Carpet rose petal\nEntertainment Area:\n- Fabric backdrop for entertainment area\n- Frame screen (styrofoam)\nVIP Area:\n- 5 pcs center piece fresh flower\n- VIP area white fences\nFoyer area:\n- 4 Angpao box and fresh flower (fabric backdrop)\n- 2 pcs souvenir cupboard\n- 1 set photobooth backdrop (styrofoam)\n- 1 set photo gallery (fabric)\nBonus:\n- 20 pcs corsage\n- 1 pc hand bouquet (rose and baby breath by White Pearl)\n- Teapai decoration (fabric - with xuang xi) or Akad Decoration\n- 1 set LED lighting by White Pearl',
          capacity: 500,
          discount: 5,
          price: 150000000,
          isActive: true,
        },
        {
          id: 'cecb6f95-baa8-4468-80eb-985558c59a1a',
          VendorId: 'd7c7c90d-5b13-4c27-96e9-cb39db4bd680',
          name: 'Quicky Sunrise traditional',
          images: [
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%2010a.png?alt=media&token=50922ac0-79f9-4375-9808-1b4c4336f011',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%2010b.png?alt=media&token=69cd9faa-5518-4317-8df1-ff8ec2b4d8bf',
            'https://firebasestorage.googleapis.com/v0/b/wedding-edfb0.appspot.com/o/products%2Fproduct%2010c.png?alt=media&token=f68f78b1-ab8e-42a2-8198-8bd663688d67',
          ],
          description:
            '- Commitment Ceremony for maximum 30 pax\n- Exclusive usage of the Skytop Wedding Chapel area for the wedding ceremony for 8 hours\n- Acrylic welcome board, blessing gate, flower petal as aisle border, flower arrangement based on request\n- Hand made impressive plated menu by the culinary master from Boheme Restaurant of set menu or buffet for 30 pax\n- Signature mocktails for all guest , non-alcoholic beverages and beers\n- 1 bottle of sparkling wine for celebrations\n- Resort sound system for background music and one microphone for the wedding ceremony with unlimited electricity provided\n- One rime romantic dinner for Bride and Groom at choosen areas\n- An indulgment of 90 mins Jani SPA Treatment for the bride and groom',
          capacity: 500,
          discount: 0,
          price: 90000000,
          isActive: true,
        },
      ],
      {}
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Products', null, {});

  }
};
