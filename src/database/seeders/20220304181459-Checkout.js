'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Checkouts',
      [
        {
          ClientId: 'd1481241-76f0-4551-be84-80fdfd98e225',
          order_id: null,
          checkout_items: null,
        },
        {
          ClientId: '63ed381c-24fe-4fd4-bb62-0b93c310f402',
          order_id: null,
          checkout_items: null,
        },
        {
          ClientId: 'd90eab4a-257d-4a33-86e3-dd0866dddd7b',
          order_id: null,
          checkout_items: null,
        },
        {
          ClientId: '59c34fe7-e568-45cf-99ee-087318f014cb',
          order_id: null,
          checkout_items: null,
        },
        {
          ClientId: '7fde95f3-dfae-4acc-939e-0989f8d62339',
          order_id: null,
          checkout_items: null,
        },
      ],
      {}
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Checkouts', null, {});

  }
};
