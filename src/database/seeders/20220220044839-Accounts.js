'use strict';
const { randomString } = require('../../utils/generator');
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Accounts',
      [
        {
          id: 'ff63edc8-b252-4e53-ac05-d0c2c585ea89',
          roles: 'VENDOR',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'vendor@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: 'a851efa8-9062-4d19-934a-4ef5de02cdc3',
          roles: 'VENDOR',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'vendor1@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '6408449c-d221-4251-815c-312ea8790795',
          roles: 'VENDOR',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'vendor2@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '8f4b6c1a-c238-4c32-9ea0-e164a6b042dd',
          roles: 'VENDOR',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'vendor3@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '87494dfd-3ba5-401b-83a5-70cc8cbd1bad',
          roles: 'VENDOR',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'vendor4@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '72d9abe5-cbb1-48d2-bc65-ce5bb2cc5685',
          roles: 'ADMIN',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'admin@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '2a1dd4a6-3726-4391-8200-08bcf1ce9eea',
          roles: 'ADMIN',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'admin1@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '7ef3b232-8e78-4f9d-9931-fdba9cd0d2fe',
          roles: 'CLIENT',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'client@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '8d88a1d9-1c9a-4906-a5b0-546f725d2e7d',
          roles: 'CLIENT',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'client1@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: 'cf3559bd-c7fe-43ca-90e9-4ae1c219ee11',
          roles: 'CLIENT',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'client2@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '5ec40c92-f42a-4364-9a87-45d69903df4b',
          roles: 'CLIENT',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'client3@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
        {
          id: '2debee63-fec7-4eaa-a41f-736710edac25',
          roles: 'CLIENT',
          isVerified: true,
          verifyCode: randomString(50),
          email: 'client4@gmail.com',
          password: '$2a$12$FwwT011r.rKUHWb8grk69OTLM39Y35fjpBA0vmmBnmjE4gJnFkMy2',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Accounts', null, {});
  },
};
