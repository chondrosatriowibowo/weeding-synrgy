'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Orders',
      [
        {
          id: 'c8695d3b-3e1e-4bda-a550-bff8a4778ff8',
          ClientId: 'd1481241-76f0-4551-be84-80fdfd98e225',
          total: 185500000,
          date: new Date("05 Januari 2021").toISOString(),
          status: 'settlement',
          payment_type: 'bank_transfer',
        },
        {
          id: 'e0756cc5-de57-4e3b-87e5-fa0c502e3b22',
          ClientId: '63ed381c-24fe-4fd4-bb62-0b93c310f402',
          total: 128000000,
          date: new Date("06 Februari 2021").toISOString(),
          status: 'settlement',
          payment_type: 'bank_transfer',
        },
        {
          id: 'c86bdd6e-b9f3-4710-9c6a-6dab20e800ed',
          ClientId: 'd90eab4a-257d-4a33-86e3-dd0866dddd7b',
          total: 18600000,
          date: new Date("12 Januari 2021").toISOString(),
          status: 'settlement',
          payment_type: 'bank_transfer',
        },
        {
          id: '83bdda0e-f5c2-4896-9ed5-2c39122a1276',
          ClientId: '59c34fe7-e568-45cf-99ee-087318f014cb',
          total: 216000000,
          date: new Date("04 Februari 2021").toISOString(),
          status: 'settlement',
          payment_type: 'bank_transfer',
        },
        {
          id: 'f57310e2-1d48-4f9a-9f52-6b409f7c52a9',
          ClientId: '7fde95f3-dfae-4acc-939e-0989f8d62339',
          total: 232500000,
          date: new Date("04 Maret 2021").toISOString(),
          status: 'settlement',
          payment_type: 'bank_transfer',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Orders', null, {});
  },
};
