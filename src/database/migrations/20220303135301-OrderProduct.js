'use strict';
const { QueryTypes } = require('sequelize');

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('OrderProducts', {
      OrderId: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      ProductId: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
      },
      price: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      discount: {
        type: Sequelize.FLOAT,
        allowNull: true,
      },
      book_date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      progress: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['UNPAID', 'CONFIRMATION', 'CANCEL', 'REFUND', 'PREPARATION', 'FINISH'],
        defaultValue: 'UNPAID',
      },
      quantity: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
    });

    await queryInterface.addConstraint('OrderProducts', {
      fields: ['OrderId'],
      type: 'foreign key',
      name: 'OrderId_fk',
      references: {
        table: 'Orders',
        field: 'id',
      },
      onUpdate: 'cascade',
    });
    await queryInterface.addConstraint('OrderProducts', {
      fields: ['ProductId'],
      type: 'foreign key',
      name: 'ProductId_fk',
      references: {
        table: 'Products',
        field: 'id',
      },
      onUpdate: 'cascade',
    });
  },

  async down(queryInterface, Sequelize) {
    return Promise.all([
      await queryInterface.dropTable('OrderProducts'),
      await queryInterface.sequelize.query('DROP TYPE IF EXISTS "enum_OrderProducts_progress";', {
        type: QueryTypes.RAW,
      })
    ]);
  },
};
