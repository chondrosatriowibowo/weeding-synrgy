'use strict';
const { QueryTypes } = require('sequelize');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`,
      {
        type: QueryTypes.RAW,
      }
    );
  },
  down: async (queryInterface, Sequelize) => {},
};
