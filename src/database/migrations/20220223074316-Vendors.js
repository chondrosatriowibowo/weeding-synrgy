'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Vendors', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
      },
      AccountId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'Accounts',
          },
          key: 'id',
        },
        onUpdate: 'cascade',
      },
      url: {
        type: Sequelize.TEXT,
        unique: true,
        allowNull: false,
      },
      isVerified: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      isBanned: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      categories: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false,
      },
      information: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      experience: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      service: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      social_media: {
        type: Sequelize.TEXT,
        defaultValue: '{facebook: "", instagram: "", twitter: ""}',
      },
      address: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      city: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      phone: {
        type: Sequelize.STRING(15),
        unique: true,
        allowNull: false,
        validate: {
          isInt: true,
        },
      },
      document: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      banner: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: '/images/banner.png'
      }
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Vendors');
  },
};
