'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Checkouts', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      ClientId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'Clients',
          },
          key: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      order_id: {
        type: Sequelize.UUID,
        allowNull: true,
      },
      checkout_items: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Checkouts');
  },
};
