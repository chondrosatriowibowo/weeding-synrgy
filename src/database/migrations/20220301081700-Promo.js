'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Promos', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: true,
        defaultValue: Sequelize.literal('uuid_generate_v4()')
      },
      ProductId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName:'Products'
          },
          key: 'id',
        },
        onDelete: 'cascade',
        onUpdate: 'cascade' 
      },
      promoCode: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      promoStartDate: {
        type:Sequelize.DATEONLY,
        allowNull:false, 
      },
      promoEndDate: {
        type:Sequelize.DATEONLY,
        allowNull:false, 
      },
      promoPercentage: {
        type:Sequelize.DOUBLE,
        allowNull:false
      },
      promoInformation: {
        type:Sequelize.STRING,
        allowNull:false
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Promos');
  }
};