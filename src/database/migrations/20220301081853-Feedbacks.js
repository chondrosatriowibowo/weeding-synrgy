'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Feedbacks', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('uuid_generate_v4()')
      },
      ClientId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName:'Clients'
          },
          key: 'id',
        },
        onUpdate: 'cascade'
      },
      ProductId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName:'Products'
          },
          key: 'id',
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      },
      title: {
        type: Sequelize.STRING,
        allowNull:false
      },
      rating: {
        type: Sequelize.INTEGER,
        allowNull:false,
        validate: {
          isInt: true,
        },
      },
      comment: {
        type: Sequelize.TEXT,
        allowNull:false
      },
      feedbackDate: {
        type: Sequelize.DATE,
        allowNull:false,
        defaultValue: Sequelize.NOW
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Feedbacks');
  }
};
