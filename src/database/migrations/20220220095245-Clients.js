'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Clients', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
      },
      AccountId: {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: {
            tableName: 'Accounts',
          },
          key: 'id',
        },
        onUpdate: 'cascade',
      },
      AccountProviderId: {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: {
            tableName: 'AccountProviders',
          },
          key: 'id',
        },
        onUpdate: 'cascade',
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false,
      },
      phone: {
        type: Sequelize.STRING(15),
        unique: true,
        allowNull: true,
        validate: {
          isInt: true,
        },
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Clients');
  },
};
