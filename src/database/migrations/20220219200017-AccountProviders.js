'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('AccountProviders', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
      },
      roles: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['VENDOR', 'CLIENT'],
        defaultValue: 'CLIENT',
      },
      avatar: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: '/images/avatar.png',
      },
      email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      providerType: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ['GOOGLE', 'FACEBOOK'],
        defaultValue: 'GOOGLE',
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('AccountProviders');
  },
};
