'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Admins', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
      },
      AccountId: {
        type: Sequelize.UUID,
        allowNull: true,
        references: {
          model: {
            tableName: 'Accounts',
          },
          key: 'id',
        },
        onUpdate: 'cascade',
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Admins');
  },
};
