/**
 * Ejs Routes
 */
const express = require('express');
const router = express.Router();
const C_VIEWS = require('../../controllers').viewsController;
const C_AUTH = require('../../controllers').authController;
const vendors = require('./vendors');

const { tokenCheckView, preventLogin } = require('../../middlewares/auth');

// Sub Ejs Routes
// const vendor = require('./vendor');
// tokenCheckView, tadinya di dibawah
router.get('/', C_VIEWS.home);
router.get('/login', preventLogin, C_VIEWS.login);
router.get('/register', preventLogin, C_VIEWS.register);
router.get('/cart', tokenCheckView, C_VIEWS.cart);
router.get('/checkout', tokenCheckView, C_VIEWS.checkout);
router.get('/profile', tokenCheckView, C_VIEWS.profile);
router.get('/order-list', tokenCheckView, C_VIEWS.orderList);
router.get('/order-detail/:id', tokenCheckView, C_VIEWS.orderDetail);
router.use('/vendors', vendors);
router.get('/products/:url', C_VIEWS.productDetail);
router.get('/products', C_VIEWS.products);
router.get('/discount', C_VIEWS.discount);
/*
 * Email Verification
 */
router.get('/verify/:verifyCode', C_AUTH.accountVerification);
/*
 * Reset Password Email Verification
 */
router.get('/reset', C_VIEWS.resetPassword);
/*
 * Update New Password After Reset Password
 */
router.get('/reset/new/:verifyCode', C_VIEWS.resetPasswordNew);
// router.use('/vendor', vendor);

module.exports = router;
