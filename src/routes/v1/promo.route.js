const express = require('express');
const router = express.Router();
const C_PROMO = require('../../controllers').promoController;

/**
 * Find All
 */
router.get('/', C_PROMO.findAllPromo);
/**
 * Find One
 */
router.get('/:id', C_PROMO.findOnePromo);
/**
 * Create
 */
router.post('/:productId', C_PROMO.createPromo);
/**
 * Update
 */
router.put('/:id', C_PROMO.updatePromo);
/**
 * Delete
 */
router.delete('/:id', C_PROMO.deletePromo);


module.exports = router;