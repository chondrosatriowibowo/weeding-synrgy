const express = require('express');
const router = express.Router();

const { filesMiddleware, fileInfoCheckOptional } = require('../../middlewares/files');
const { onLogin, tokenCheckAPI } = require('../../middlewares/auth');
const { csrfGoogle } = require('../../middlewares/csrf');

const C_AUTH = require('../../controllers').authController;

/**
 * JWT CHECK
 */
router.get('/token-check', tokenCheckAPI, C_AUTH.checkPayload);

/*
 * REGISTER
 */
router.post('/register', filesMiddleware, fileInfoCheckOptional, C_AUTH.register);

/*
 * REGISTER WITH PROVIDER
 */
router.post('/register/provider', C_AUTH.registerProvider);

/*
 * LOGIN
 */
router.post('/login', onLogin, C_AUTH.login);

/*
 * LOGIN WITH PROVIDER
 */
router.post('/provider', C_AUTH.loginProvider);

/*
 * LOGOUT
 */
//TODO: MDL for logout
router.get('/logout', C_AUTH.logout);

/*
 * RESET PASSWROD EMAIL
 */
router.post('/reset', C_AUTH.forgotPassword);

module.exports = router;
