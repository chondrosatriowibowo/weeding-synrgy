const express = require('express');
const router = express.Router();
const { filesMiddleware, fileInfoCheck, uploadImages, fileInfoCheckRequired, fileInfoCheckOptional } = require('../../middlewares/files');
const { tokenCheckAPI } = require('../../middlewares/auth');

const C_PRODUCT = require('../../controllers').productController;

/**
 * Find All
 */
router.get('/', C_PRODUCT.findAllProduct);
/**
 * Find One
 */
router.get('/:id', C_PRODUCT.findOne);
/**
 * Find Discount Product
 */
 router.get('/all/discount', C_PRODUCT.findDiscountProduct);
/**
 * Find Relationship with Feedback
 */
router.get('/r/productfeedback', C_PRODUCT.findAllFeedback);
/**
 * Find Relationship with Vendor
 */
 router.get('/r/:vendorId', C_PRODUCT.findAllProductRelVendor);
/**
* Create
*/
router.post('/', tokenCheckAPI, filesMiddleware, fileInfoCheckRequired, C_PRODUCT.createProduct);
/**
 * Find Cart Product
 */
router.post('/cart', C_PRODUCT.findProductCart);
/**
 * Update
 */
router.put('/:id', tokenCheckAPI, filesMiddleware, fileInfoCheckOptional, C_PRODUCT.updateProduct);
/**
 * Delete
 */
router.delete('/:id', tokenCheckAPI, C_PRODUCT.deleteProduct);

module.exports = router;
