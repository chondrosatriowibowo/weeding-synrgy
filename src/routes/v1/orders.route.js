const express = require('express');
const router = express.Router();

const C_ORDER = require('../../controllers').orderController;

/**
 * Create Transaction
 */
router.post('/checkout', C_ORDER.checkout);
/**
 * On Finish Hooks
 */
router.get('/finish', C_ORDER.onFinish);
router.post('/finish', C_ORDER.onFinish);
/**
 * Notification Hooks, Create Order and Update Status Order here
 */
router.post('/notification', C_ORDER.notificationHooks);
/**
 * Get All Orders Relation with Product
 */
router.get('/r/products', C_ORDER.findAllRelProduct);
/**
 * Get One Order Relation with Other Models
 */
router.get('/r/products/:id', C_ORDER.findOneRelProduct);
/**
 * Get All OrderProduct by User ID
 */
router.get('/r/order-products', C_ORDER.findAllOrderProductUser);
/**
 * Update Progress in OrderProduct
 */
router.put('/progress', C_ORDER.updateProgress);
/**
 * Status Payment
 */
router.get('/status/payment/:id', C_ORDER.statusPayment);
/**
 * Update Status Payment
 */
router.put('/status/payment/:id', C_ORDER.updateStatusPayment);
/**
 * Unfinish Hooks
 */
router.post('/unfinish', C_ORDER.onFinish);


module.exports = router;
