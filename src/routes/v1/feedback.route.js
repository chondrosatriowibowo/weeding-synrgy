const express = require('express');
const router = express.Router();
const C_FEEDBACK = require('../../controllers').feedbackController;
const { tokenCheckAPI } = require('../../middlewares/auth');

/**
 * Find All
 */
router.get('/:vendorId', C_FEEDBACK.findAllFeedback);
/**
 * Find Fendor rating
 */
router.get('/rating/:vendorId', C_FEEDBACK.findVendorRating);
/**
 * Find One
 */
router.get('/:id', C_FEEDBACK.findOneFeedback);
/**
 * Create
 */
router.post('/:productId', tokenCheckAPI, C_FEEDBACK.createFeedback);
/**
 * Update
 */
router.put('/:id', tokenCheckAPI ,C_FEEDBACK.updateFeedback);
/**
 * Delete
 */
router.delete('/:id', tokenCheckAPI, C_FEEDBACK.deleteFeedback);

   module.exports = router;