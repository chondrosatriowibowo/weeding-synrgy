const express = require('express');
const router = express.Router();
const C_CHECKOUT = require('../../controllers').checkoutController;

router.get('/', C_CHECKOUT.findAll);
router.get('/items', C_CHECKOUT.findCheckoutItems);
router.post('/create', C_CHECKOUT.createCheckout);

module.exports = router;
