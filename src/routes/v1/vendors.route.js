const express = require('express');
const router = express.Router();
const { filesMiddleware, fileInfoCheckRequired, fileInfoCheckOptional } = require('../../middlewares/files');

const C_VENDOR = require('../../controllers').vendorController;
const { tokenCheckAPI } = require('../../middlewares/auth');

/**
 * Upload File Firebase Test
 */
// Upload file
router.post('/', filesMiddleware, fileInfoCheckRequired, C_VENDOR.createTest);
// Delete file
router.delete('/file/delete', C_VENDOR.deleteTest);

/**
 * Find All
 */
router.get('/', C_VENDOR.findAll);
/**
 * Find One By URL (Vendor Profile URL)
 */
router.get('/:profile', C_VENDOR.findOneByURL);
/**
 * Find One By ID
 */
router.get('/id/:id', C_VENDOR.findOneByID);
/**
 * Find All Relationship with Account
 */
router.get('/r/accounts', C_VENDOR.findAllRelAccount);
/**
 * Find One Relationship with Account
 */
router.get('/r/accounts/:id', C_VENDOR.findOneRelAccount);
/**
 * Find One Detail Vendor Head
 */
router.get('/detail-vendor/:id', C_VENDOR.findOneDetailVendorHead);
/**
 * Update
 */
router.put('/:id', tokenCheckAPI, filesMiddleware, fileInfoCheckOptional, C_VENDOR.update);
/**
 * Delete
 */
router.delete('/:id', tokenCheckAPI, C_VENDOR.delete);

module.exports = router;
