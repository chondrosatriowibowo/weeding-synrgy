const express = require('express');
const router = express.Router();
const C_CLIENT = require('../../controllers').clientController;

/**
 * Find All Clients
 */
router.get('/', C_CLIENT.findAll);
/**
 * Find All Clients Relationship with Account
 */
router.get('/r/accounts', C_CLIENT.findAllRelAccount);
/**
 * Find One Client Relationship with Account by ID
 */
router.get('/r/accounts/:id', C_CLIENT.findOneRelAccount);

module.exports = router;
