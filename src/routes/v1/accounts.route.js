const express = require('express');
const router = express.Router();
const C_USER = require('../../controllers').accountController;
const { csrfGoogle } = require('../../middlewares/csrf');

router.get('/', C_USER.findAll);
/**
 * UPDATE RESET PASSWORD
 */
router.post('/reset/:verifyCode', C_USER.resetPassword);
/**
 * Create new account, if provider is exist
 * and user haven't setup email password account
 */
router.post('/update', C_USER.update);
/**
 * Create new Account Provider, if AccountProviderId is not exist in Client or Vendor
 */
router.post('/connect/provider', csrfGoogle, C_USER.connectAccountProvider);

module.exports = router;
