const express = require('express');
const router = express.Router();

const auth = require('./auth.route');
const accounts = require('./accounts.route');
const clients = require('./clients.route');
const vendors = require('./vendors.route');
const product = require('./product.route');
const orders = require('./orders.route');
const feedback = require('./feedback.route');
const promo = require('./promo.route');
const checkouts = require('./checkouts.route');

const { tokenCheckAPI } = require('../../middlewares/auth');

router.use('/auth', auth);
router.use('/accounts', accounts);
router.use('/clients', tokenCheckAPI, clients);
router.use('/vendors', vendors);
router.use('/product', product);
router.use('/orders', orders);
router.use('/feedback', feedback);
router.use('/promo', promo);
router.use('/checkouts', tokenCheckAPI, checkouts);

module.exports = router;
