function socketApp(io) {
  const checkoutHandler = () => {
    io.emit('checkout');
  }

  io.on('connection', (socket) => {
    console.log('Client connected');
    socket.on('disconnect', () => {
      socket.on('disconnect', () => console.log('Client disconnected'));
    });
  });
  setInterval(() => io.emit('time', new Date().toTimeString()), 1000);
}

module.exports = socketApp;
