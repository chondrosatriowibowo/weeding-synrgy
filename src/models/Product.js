'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Product.belongsTo(models.Vendor)
      Product.hasMany(models.Feedback)
      Product.hasOne(models.Promo)
      Product.belongsToMany(models.Order, { through: models.OrderProduct });
      Product.hasMany(models.OrderProduct);
    }
  }
  Product.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
    },
    VendorId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Vendor',
        key: 'id',
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    images: {
      type:DataTypes.ARRAY(DataTypes.TEXT),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    capacity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    discount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
  },
  {
    sequelize,
    timestamps: false,
    modelName: 'Product',
  });
  return Product;
};
