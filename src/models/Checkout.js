'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Checkout extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Checkout.belongsTo(models.Client);
    }
  }
  Checkout.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      ClientId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'Client',
          key: 'id',
        },
      },
      order_id: {
        type: DataTypes.UUID,
        allowNull: true,
      },
      checkout_items: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
    },
    {
      sequelize,
      timestamps: false,
      modelName: 'Checkout',
    }
  );
  // Checkout.removeAttribute('id');
  return Checkout;
};
