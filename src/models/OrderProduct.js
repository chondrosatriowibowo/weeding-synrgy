'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class OrderProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      OrderProduct.belongsTo(models.Order);
      OrderProduct.belongsTo(models.Product);
    }
  }
  OrderProduct.init(
    {
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      discount: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      book_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      progress: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['UNPAID', 'CONFIRMATION', 'CANCEL', 'REFUND', 'PREPARATION', 'FINISH'],
        defaultValue: 'UNPAID',
      },
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      timestamps: false,
      modelName: 'OrderProduct',
    }
  );
  return OrderProduct;
};
