'use strict';
const { Model } = require('sequelize');
const { randomString } = require('../utils/generator');
module.exports = (sequelize, DataTypes) => {
  class Vendor extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Vendor.hasMany(models.Product)
      Vendor.belongsTo(models.Account);
    }
  }
  Vendor.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      AccountId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'Account',
          key: 'id',
        },
      },
      url: {
        type: DataTypes.TEXT,
        unique: true,
        allowNull: true,
      },
      isVerified: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      isBanned: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      categories: {
        type: DataTypes.ARRAY(DataTypes.TEXT),
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      information: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      experience: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      service: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      social_media: {
        type: DataTypes.TEXT,
        defaultValue: '{facebook: "", instagram: "", twitter: ""}',
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      city: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(15),
        unique: true,
        allowNull: false,
        validate: {
          isInt: true,
        },
      },
      document: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      banner: {
        type: DataTypes.TEXT,
        allowNull: true,
        defaultValue: '/images/banner.png'
      }
    },
    {
      sequelize,
      timestamps: false,
      modelName: 'Vendor',
      hooks: {
        beforeCreate: (vendor, options) => {
          vendor.url = `${vendor.name.split(' ')[0]}-${randomString(5)}`.toLowerCase();
        },
      },
    }
  );
  return Vendor;
};
