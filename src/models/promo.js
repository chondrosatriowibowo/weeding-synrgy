'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Promo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Promo.belongsTo(models.Product)
    }
  }
  Promo.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
    },
    ProductId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Product',
        key: 'id',
      },
    },
    promoCode: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    promoStartDate: {
      type:DataTypes.DATEONLY,
      allowNull:false, 
    },
    promoEndDate: {
      type:DataTypes.DATEONLY,
      allowNull:false, 
    },
    promoPercentage: {
      type:DataTypes.DOUBLE,
      allowNull:false
    },
    promoInformation: {
      type:DataTypes.STRING,
      allowNull:false
    }
  }, {
    sequelize,
    timestamps: false,
    modelName: 'Promo',
    hooks: {
      beforeCreate: (promo, options) =>{
        if(promo.promoStartDate > promo.promoEndDate) {
          throw new Error('Error')
        }
      }
    }
  });
  return Promo;
};