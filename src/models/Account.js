'use strict';
const { Model } = require('sequelize');
let { randomString } = require('../utils/generator');
module.exports = (sequelize, DataTypes) => {
  class Account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Account.hasOne(models.Admin);
      Account.hasOne(models.Client);
      Account.hasOne(models.Vendor);
    }
  }
  Account.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      roles: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['ADMIN', 'VENDOR', 'CLIENT'],
        defaultValue: 'CLIENT',
      },
      isVerified: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      verifyCode: {
        type: DataTypes.TEXT,
        unique: true,
        allowNull: true,
      },
      avatar: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '/images/avatar.png',
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
        notEmpty: {
          msg: 'Email is required',
        },
      },
      password: {
        type: DataTypes.TEXT,
        allowNull: false,
        notEmpty: {
          msg: 'Password is required',
        },
      },
    },
    {
      sequelize,
      timestamps: false,
      modelName: 'Account',
      hooks: {
        beforeCreate: (account, options) => {
          account.verifyCode = randomString(50);
        },
      },
    }
  );
  return Account;
};
