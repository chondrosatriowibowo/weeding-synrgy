'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Feedback.belongsTo(models.Client)
      Feedback.belongsTo(models.Product)
    }
  }
  Feedback.init({
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
    },
    ClientId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model:'Client',
        key:'id'
        },
      },
    ProductId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model:'Product',
        key:'id'
        },
      },
    rating: {
      type: DataTypes.NUMBER,
      allowNull:false
    },
    title: {
      type: DataTypes.STRING,
      allowNull:false
    },
    comment: {
      type: DataTypes.TEXT,
      allowNull:false
    },
    feedbackDate: {
      type: DataTypes.DATE,
      allowNull:false,
      defaultValue: DataTypes.NOW,
    },
  }, {
    sequelize,
    timestamps: false,
    modelName: 'Feedback',
  });
  return Feedback;
};
