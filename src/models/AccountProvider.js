'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AccountProvider extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      AccountProvider.hasOne(models.Client);
    }
  }
  AccountProvider.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      roles: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['VENDOR', 'CLIENT'],
        defaultValue: 'CLIENT',
      },
      avatar: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '/images/avatar.png',
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
        notEmpty: {
          msg: 'Email is required',
        },
      },
      providerType: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['GOOGLE', 'FACEBOOK'],
        defaultValue: 'GOOGLE',
      },
    },
    {
      sequelize,
      timestamps: false,
      modelName: 'AccountProvider',
    }
  );
  return AccountProvider;
};
