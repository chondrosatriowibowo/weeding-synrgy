'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Client extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Client.belongsTo(models.Account);
      Client.belongsTo(models.AccountProvider);
      Client.hasOne(models.Feedback);
      Client.hasMany(models.Order);
      Client.hasOne(models.Checkout);
    }
  }
  Client.init(
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      AccountId: {
        type: DataTypes.UUID,
        allowNull: true,
        references: {
          model: 'Account',
          key: 'id',
        },
      },
      AccountProviderId: {
        type: DataTypes.UUID,
        allowNull: true,
        references: {
          model: 'AccountProvider',
          key: 'id',
        },
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(15),
        unique: true,
        allowNull: true,
        validate: {
          isInt: true,
        },
      },
    },
    {
      sequelize,
      timestamps: false,
      modelName: 'Client',
    }
  );
  return Client;
};
