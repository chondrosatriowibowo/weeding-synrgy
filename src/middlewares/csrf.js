const errorMsg = require('../utils/errorMessage');
/**
 * CSRF protection middleware
 */
const csrfGoogle = (req, res, next) => {
  // Get CSRF token from cookies
  const csrf = req.cookies.g_csrf_token;
  // Get token from request
  const csrfBody = req.body.g_csrf_token;
  // Check if token cookies exists
  if(!csrf) {
    return next(errorMsg(400, null, 'No CSRF token in Cookie'));
  }
  // Check if token cookies exists
  if(!csrfBody) {
    return next(errorMsg(400, null, 'No CSRF token in post body'));
  }
  // Check if token not same
  if (csrf !== csrfBody) {
    return next(errorMsg(400, null, 'Failed to verify double submit cookie'));
  }
  // CSRF token is valid
  return next();
};

module.exports = {
  csrfGoogle,
};
