const multer = require('multer');
const errMsg = require('../utils/errorMessage');
/**
 * Storage type multer (DiskStorage)
 */
const storageMulter = multer.diskStorage({
  // Rename original file
  filename: (req, file, cb) => {
    cb(null, file.fieldname + file.originalname + '-' + Date.now());
  },
});
const justStorage = multer.diskStorage({
  filename: (req, file, cb) =>{
    cb(null, file.originalname)
  }
})
/**
 * Middleware multer (FileFilter)
 */
const filesMiddleware = multer({ storageMulter })
  .fields([
    {
      // Field name
      name: 'vendor_image',
      // Max count file(s)
      maxCount: 1,
    },
    {  name: 'banner', maxCount: 1 },
    {name: "images", maxCount: 3},
    { name: 'avatar', maxCount: 1},
    { name: 'document', maxCount: 1},
  ])
/**
 *
 * @param {boolean} isRequired - Check if file check is needed or not
 * @returns {any}
 */
const fileInfoCheck = (isRequired) => (req, res, next) => {
  // Check if files are empty
  if (!req.files) {
    return isRequired ? next(errMsg(400, {}, 'File is empty')) : next();
  }
  // Get all key files
  const files = Object.keys(req.files);
  // Check file size not exceed 5MB
  files.forEach((file) => {
    if (req.files[file][0].size > 5000000) {
      return next(errMsg(400, {}, 'File size is too large'));
    }
  });
  // Next to controller
  return next();
}
/**
 * File information checking (Required)
 */
const fileInfoCheckRequired = fileInfoCheck(true);
/**
 * File information checking (Optional)
 */
const fileInfoCheckOptional = fileInfoCheck(false);

module.exports = {
  filesMiddleware,
  fileInfoCheckRequired,
  fileInfoCheckOptional,
};
