/**
 * Random string generator
 * @param {number} len - Length characters
 * @returns {string}
 */
const randomString = (len) => {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  return [...Array(len)].map(() => chars.charAt(Math.floor(Math.random() * chars.length))).join('');
}

module.exports = {
  randomString,
}
