/**
 * MIME types check for file upload
 * @param {String} type - current file MIME type
 * @param {'pdf' | 'image'} targetType - target file type
 */
const mimeTypeCheck = (type, targetType) => {
  const splittedType = type.split('/', 1)[0];
  switch (targetType) {
    case 'pdf':
      if(type !== 'application/pdf')
        throw new Error('File type is not pdf');
      break;
    case 'image':
      if(splittedType !== 'image')
        throw new Error('File type is not image');
      break;
    default:
      throw new Error('File type is not found');
  }
  return true;
}

module.exports = {
  mimeTypeCheck,
}
