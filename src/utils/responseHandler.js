const service = require('../services/service');

/**
 * Response Handler
 * * example: `responseHandler(res, 200, data);`
 * @param {any} res - response object
 * @param {number} code - status code
 * @param {any} data - payload
 * @returns response object `{ code, data }`
 *
 */
const responseHandler = (res, code, data) => res.status(code).send({ code, data });
/**
 *
 * @param {*} res
 * @param {*} code
 * @param {*} data
 * @returns
 */
const paginationHandler = (req, res, code, data) => {
  const url = `${req.protocol}://${req.hostname}${req.originalUrl}`;
  let { offset, limit } = req.query;
  let nextPage = new URL(url), previousPage = new URL(url);
  const totalPage = service.totalPage(data.count, limit);
  // Set cursor to next and previous page
  const updateURL = (pageURL, type) => {
    // Check if offset and limit is null
    if (!offset || !limit) {
      // Get offset and limit from default value service
      offset = service.offset;
      limit = service.limit;
      previousPage = null;
    }
    // Reset query offset & limit
    let countOffset = type === 'next' ? Number(offset) + Number(limit) : Number(offset) - Number(limit);
    // Don't show next page if total offset is greater than total data
    if(countOffset > data.count) nextPage = null;
    // Update query url
    pageURL.searchParams.delete('offset');
    pageURL.searchParams.delete('limit');
    pageURL.searchParams.set('offset', countOffset);
    pageURL.searchParams.set('limit', '' + limit);
  }
  // Check if total data is less than limit
  if(data.count < limit) {
    // No cursor to next and previous page
    previousPage = null;
    nextPage = null;
  } else {
    if(offset === 0) {
      previousPage = null;
    }
    else {
      // Set cursor to previous page
      updateURL(previousPage, 'prev');
      // Set cursor to next page
      updateURL(nextPage, 'next');
      // Check if current page is last page
      if(offset >= data.count) {
        // No cursor to next page
        nextPage = null;
      }
    }
  }
  res.status(code).send({ code, data: { totalPage, nextPage, previousPage, ...data } });
};

module.exports = { responseHandler, paginationHandler };
