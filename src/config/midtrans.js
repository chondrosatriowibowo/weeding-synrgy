require('dotenv').config();
const config = {
  merchant_id: process.env.MT_MERCHANT_ID,
  client_key: process.env.MT_CLIENT_KEY,
  server_key: process.env.MT_SERVER_KEY,
};
const authorization = Buffer.from(`${config.server_key}:`).toString('base64');
const endpoint = {
  default: 'https://app.sandbox.midtrans.com',
  v1: 'https://app.sandbox.midtrans.com/snap/v1',
  v2: 'https://api.sandbox.midtrans.com/v2',
  v3: 'https://api.sandbox.midtrans.com/v3',
};

module.exports = {
  config,
  authorization,
  endpoint,
};
