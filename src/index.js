const app = require('./app');
// const socketIO = require('socket.io');
// const socketApp = require('./socket');
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Listening to port ${port}`);
});
// socketApp(socketIO(server));
