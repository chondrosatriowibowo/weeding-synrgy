const AccountsService = require('../services/accounts.service');
const AccountProvidersService = require('../services/accountProviders.service');
const { AuthService } = require('../services/firebase/firebase.service');

const jwt = require('../utils/jwt');
const bcrypt = require('../utils/bcrypt');
const { randomString } = require('../utils/generator');
const { responseHandler } = require('../utils/responseHandler');

class AccountsController {
  static async findAll(req, res, next) {
    try {
      const data = await AccountsService.findAll();
      return responseHandler(res, 200, data);
    } catch (error) {
      res.status(400);
      next(error);
    }
  }

  /**
   * Update new password after reset password
   */
  static async resetPassword(req, res, next) {
    const { verifyCode } = req.params;
    const isVerified = JSON.parse(req.query.isVerified);

    try {
      // Hash password
      const password = await bcrypt.hash(req.body.password);
      // Generate new verify code (not used)
      const newVerifyCode = randomString(5);
      // Update password
      await AccountsService.updateWhereOptions(
        // If verified, generate new verify code
        isVerified ? { verifyCode: newVerifyCode, password } : { password },
        // Where options
        { verifyCode }
      );
      req.flash('info', 'Password updated successfully');
      return res.redirect('/login');
    } catch (error) {
      req.flash('error', 'Password updated failed');
      return res.redirect('/login');
    }
  }

  /**
   * Create new account, if provider is exist
   * and user haven't setup email password account
   */
  static async update(req, res, next) {
    const { redirect } = req.query;

    try {
      // Email: could be in Accounts or AccountProviders
      const user = jwt.getPayloadCookies(req);
      // Get email from Accounts
      const accountEmail = await AccountsService.getEmail(user.email);
      const data = await AccountsService.update(accountEmail, user, req.body);
      req.flash('info', 'Profile updated successfully');
      return redirect ? res.redirect('/profile') : responseHandler(res, 200, data);
    } catch (error) {
      req.flash('error', error.message);
      return redirect ? res.redirect('/profile') : next(errMsg(400, error, error.message));
    }
  }

  /**
   * Create new Account Provider, if AccountProviderId is not exist in Client or Vendor
   */
  static async connectAccountProvider(req, res, next) {
    const { redirect } = req.query;

    try {
      // Payload from cookies
      const user = jwt.getPayloadCookies(req);
      // Token from google sign in
      const g_token = await AuthService.signInWithGoogle(req.body.credential);
      // Account provider from database
      let account = await AccountProvidersService.findOneAuth(g_token.email);
      if (!account) {
        // Update Client with Provider Account
        account = await AccountProvidersService.connectToProvider(user, g_token);
      } else {
        throw new Error('Account already exist');
      }
      req.flash('info', 'Connected to Google successfully');
      return redirect ? res.redirect('/profile') : responseHandler(res, 200, account);
    } catch (error) {
      req.flash('error', error.message);
      return redirect ? res.redirect('/profile') : next(errMsg(400, error, error.message));
    }
  }
}

module.exports = AccountsController;
