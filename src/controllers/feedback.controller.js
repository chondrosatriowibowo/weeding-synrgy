const FeedbackService = require('../services/feedback.service');
const jwt = require('../utils/jwt');

const { responseHandler, paginationHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');

class FeedbackController {

    static async findAllFeedback(req, res, next){
        try {
            const {offset, limit} = req.query
            const data = await FeedbackService.findAllFeedback(offset, limit, req.params.vendorId)
            return paginationHandler(req, res, 200, data);
        } catch (error) {
            next(errMsg(400, error, error.message));
        }
    }

    static async findVendorRating(req, res, next){
        try {
            const data = await FeedbackService.findVendorRating(req.params.vendorId)
            return responseHandler(res, 200, data);
        } catch (error) {
            next(errMsg(400, error, error.message));
        }
    }

    static async findOneFeedback(req, res, next){
        try {
            const { id } = req.params
            const data = await FeedbackService.findOne(id)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }
    static async createFeedback(req, res, next){
      const { redirect, vendorId } = req.query;

        try {
            const user = jwt.getPayloadCookies(req);
            const data = await FeedbackService.createFeedback(user.profileId, req.params, req.body)
            return redirect ? res.redirect('/vendors/' + vendorId) : responseHandler(res, 200, data);
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }
    static async updateFeedback(req, res, next){
        try {
            const { id } = req.params
            const data = await FeedbackService.update(id, req.body)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }
    static async countFeedback(req, res, next){
        try {
            const data = await FeedbackService.countFeedback()
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }
    static async deleteFeedback(req, res, next){
        try {
            const { id } = req.params
            const data = await FeedbackService.remove(id)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }
}

module.exports = FeedbackController
