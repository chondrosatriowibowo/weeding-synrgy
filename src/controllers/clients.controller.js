const ClientsService = require('../services/clients.service');
const { responseHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');
const { getPayloadCookies } = require('../utils/jwt');

class ClientsController {
  static async findAll(req, res, next) {
    try {
      const data = await ClientsService.findAll();
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findAllRelAccount(req, res, next) {
    try {
      const data = await ClientsService.findAllRelAccount();
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findOneRelAccount(req, res, next) {
    const { id } = req.params;

    try {
      const data = await ClientsService.findOneRelAccount(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
}

module.exports = ClientsController;
