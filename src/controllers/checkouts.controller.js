const CheckoutsService = require('../services/checkouts.service');
const { responseHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');
const { getPayloadCookies } = require('../utils/jwt');

class CheckoutsController {
  static async findAll(req, res, next) {
    try {
      const { profileId } = getPayloadCookies(req);
      const data = await CheckoutsService.findAllRelProducts(profileId);
      return responseHandler(res, 200, data);
    } catch (error) {
      req.flash('error', 'Failed to checkout data');
      return next(errMsg(400, error, error.message));
    }
  }

  static async findCheckoutItems(req, res, next) {
    try {
      const { profileId } = getPayloadCookies(req);
      const data = await CheckoutsService.findCheckoutItems(profileId);
      return responseHandler(res, 200, data);
    } catch (error) {
      req.flash('error', 'Failed to checkout data');
      return next(errMsg(400, error, error.message));
    }
  }

  /**
   * Create or update Checkout
   */
  static async createCheckout(req, res, next) {
    const { checkout_items } = req.body;
    try {
      const { profileId } = getPayloadCookies(req);
      const data = await CheckoutsService.createCheckout(profileId, checkout_items);
      return responseHandler(res, 200, data);
    } catch (error) {
      return next(errMsg(400, error, error.message));
    }
  }
}

module.exports = CheckoutsController;
