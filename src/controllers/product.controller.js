const ProductService = require('../services/product.service');
const { StorageService } = require('../services/firebase/firebase.service');
const jwt = require('../utils/jwt');

const { responseHandler, paginationHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');

class ProductController {
  static async findAllProduct(req, res, next) {
    const { offset, limit } = req.query;
    try {
      let data;
      if (req.query) {
        data = await ProductService.findProductByQuery(offset, limit, req.query);
      } else {
        data = await ProductService.findAll();
      }
      return paginationHandler(req, res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
  static async findAllProductRelVendor(req, res, next) {
    const { offset, limit } = req.query;
    const { vendorId } = req.params;

    try {
      const data = await ProductService.findProductRelVendor(offset, limit, vendorId);
      return paginationHandler(req, res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findOne(req, res, next) {
    const { id } = req.params;
    try {
      const data = await ProductService.findOneProduct(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Find product cart
   * @param {Array<{id: string, quantity: number}>} items - items from cart
   */
  static async findProductCart(req, res, next) {
    const { checkout_items } = req.body;

    try {
      const data = await ProductService.findProductCart(checkout_items);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findDiscountProduct(req, res, next) {
    const { offset, limit } = req.query;
    try {
      const data = await ProductService.findDiscountProduct(offset, limit);
      return paginationHandler(req, res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findAllFeedback(req, res, next) {
    try {
      const data = await ProductService.findAllFeedback();
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
  static async createProduct(req, res, next) {
    try {
      let data = await ProductService.createProduct(req.files, req.body);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async updateProduct(req, res, next) {
    try {
      const data = await ProductService.updateProduct(req.params, req.files, req.body);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
  static async deleteProduct(req, res, next) {
    const { id } = req.params;
    try {
      const data = await ProductService.remove(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
}

module.exports = ProductController
