const VendorsService = require('../services/vendors.service');
const { StorageService } = require('../services/firebase/firebase.service');

const jwt = require('../utils/jwt');
const { responseHandler, paginationHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');

class VendorsController {
  /**
   * Upload Firebase file test
   */
  static async createTest(req, res, next) {
    try {
      const { vendor_image } = req.files;
      const url = await StorageService.upload('profile', vendor_image[0], 'image');
      return responseHandler(res, 200, url);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
  /**
   * Delete Firebase file test
   */
  static async deleteTest(req, res, next) {
    try {
      const { url } = req.body;
      const deleteFile = await StorageService.delete(url);
      return responseHandler(res, 200, deleteFile);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findAll(req, res, next) {
    const { offset, limit } = req.query;

    try {
      let data = await VendorsService.findAllMultiWhereOptions(offset, limit, req.query);
      return paginationHandler(req, res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Find One By URL (Vendor Profile URL)
   */
  static async findOneByURL(req, res, next) {
    const { profile } = req.params;

    try {
      const data = await VendorsService.findOneWhereOptions({ url: profile });
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Detail Vendor Head Section
   */
  static async findOneDetailVendorHead(req, res, next) {
    const { id } = req.params;

    try {
      const data = await VendorsService.detailVendorHead(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Find One By ID
   */
  static async findOneByID(req, res, next) {
    const { id } = req.params;

    try {
      const data = await VendorsService.findOne(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findAllRelAccount(req, res, next) {
    try {
      const data = await VendorsService.findAllRelAccount();
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async findOneRelAccount(req, res, next) {
    const { id } = req.params;

    try {
      const data = await VendorsService.findOneRelAccount(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async update(req, res, next) {
    const { id } = req.params;

    try {
      console.log(req.files)
      const user = jwt.getPayloadCookies(req);
      // Check if user is not admin
      if(user.roles !== 'ADMIN') {
        // Disable to update vendor verified status
        delete req.body.isVerified;
        // Disable to update vendor banned status
        delete req.body.isBanned;
      }
      // Parse categories into array if exist
      if(req.body.categories) {
        req.body.categories = JSON.parse(req.body.categories);
      }
      const data = await VendorsService.update(id, req.body, req.files);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static async delete(req, res, next) {
    const { id } = req.params;

    try {
      const data = await VendorsService.remove(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
}

module.exports = VendorsController;
