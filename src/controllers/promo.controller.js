const PromoService = require('../services/promo.service');
const jwt = require('../utils/jwt');

const { responseHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');

class promoController {

    static async findAllPromo(req, res, next){
        try {
            const data = await PromoService.findAll()
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }

    static async findOnePromo(req, res, next){
        try {
            const {id} = req.params
            const data = await PromoService.findOne(id)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }

    static async createPromo(req, res, next){
        try {
            console.log(req.body)
            const data = await PromoService.createPromo(req.params, req.body)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }

    static async updatePromo(req, res, next){
        try {
            const {id} = req.params
            const data = await PromoService.update(id, req.body)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }

    static async deletePromo(req, res, next){
        try {
            const { id } = req.params
            const data = await PromoService.remove(id)
            return responseHandler(res, 200, data)
        } catch (error) {
            next(errMsg(400, error, error.message))
        }
    }
}

module.exports = promoController
