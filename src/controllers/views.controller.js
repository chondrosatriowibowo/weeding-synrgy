const template = require('../utils/renderEjs');
const { getPayloadCookies } = require('../utils/jwt');

const AccountsService = require('../services/accounts.service');
const AccountProvidersService = require('../services/accountProviders.service');
const CheckoutsService = require('../services/checkouts.service');
const ClientsService = require('../services/clients.service');
const OrdersService = require('../services/orders.service');
const PaymentsService = require('../services/payment/payment.service');
const VendorsService = require('../services/vendors.service');

class ViewsController {
  static register(req, res) {
    const errorMsg = req.flash('error');
    // Uncomment below, if you want to customize your own index page
    // res.indexPage = 'pages/your_page';
    template(res, {
      page: 'register',
      props: {
        title: 'Register',
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static login(req, res) {
    const errorMsg = req.flash('error');
    const infoMsg = req.flash('info');
    template(res, {
      page: 'login',
      props: {
        title: 'Login',
        info: infoMsg[0] ?? null, // Info message
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static home(req, res) {
    template(res, {
      page: 'home',
      user: getPayloadCookies(req),
      props: {
        title: 'Home',
      },
    });
  }

  static resetPassword(req, res) {
    const errorMsg = req.flash('error');
    const infoMsg = req.flash('info');
    template(res, {
      page: 'reset/index',
      props: {
        title: 'Reset Password',
        info: infoMsg[0] ?? null, // Info message
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static async resetPasswordNew(req, res) {
    try {
      const { verifyCode } = req.params;
      const errorMsg = req.flash('error');
      const infoMsg = req.flash('info');
      const data = await AccountsService.findOneWhereOptions({ verifyCode });

      template(res, {
        page: 'reset/confirmation',
        success: true,
        props: {
          title: 'Update New Password',
          info: infoMsg[0] ?? null, // Info message
          error: errorMsg[0] ?? null, // Error message
          isVerified: data.isVerified,
          verifyCode,
        },
      });
    } catch (error) {
      template(res, {
        page: 'reset/confirmation',
        success: false,
        props: {
          title: 'Update New Password',
        },
      });
    }
  }

  static cart(req, res) {
    template(res, {
      page: 'cart',
      props: {
        title: 'Cart',
      },
    });
  }

  static async checkout(req, res) {
    const user = getPayloadCookies(req);
    const data = await CheckoutsService.findCheckoutItems(user.profileId);

    template(res, {
      page: 'checkout',
      user,
      props: {
        data: data ?? [],
        title: 'Checkout',
      },
    });
  }

  static async profile(req, res) {
    const errorMsg = req.flash('error');
    const infoMsg = req.flash('info');
    const user = getPayloadCookies(req);
    const data = await ClientsService.findOneRelAccount(user.profileId);
    // Set is connected provider
    user['isConnectedProvider'] = await AccountProvidersService.isConnectedProvider(user);

    template(res, {
      page: 'profile',
      user,
      props: {
        title: 'Profile',
        data,
        info: infoMsg[0] ?? null, // Info message
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static async orderList(req, res) {
    const errorMsg = req.flash('error');
    const infoMsg = req.flash('info');
    const user = getPayloadCookies(req);
    const data = (await OrdersService.findAllOrderProductByUserId(user.profileId, req.query)) ?? [];

    template(res, {
      page: 'orders/order-list',
      user,
      props: {
        title: 'Order List',
        data,
        info: infoMsg[0] ?? null, // Info message
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static async vendors(req, res) {
    const errorMsg = req.flash('error');
    const infoMsg = req.flash('info');
    const user = getPayloadCookies(req);

    template(res, {
      page: 'vendors',
      user,
      props: {
        title: 'Vendors',
        info: infoMsg[0] ?? null, // Info message
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static async vendorDetail(req, res) {
    const { id } = req.params;
    // Data head section
    const data = await VendorsService.detailVendorHead(id);
    const user = getPayloadCookies(req);

    template(res, {
      page: 'detailvendor/index',
      user,
      props: {
        title: 'Detail Vendor',
        data,
      },
    });
  }

  static async productDetail(req, res) {
    const { url } = req.params;
    // Data head section
    const data = await VendorsService.detailVendorHead(url);
    const user = getPayloadCookies(req);

    template(res, {
      page: 'detailproduct/index',
      user,
      props: {
        title: 'Detail Product',
        data,
      },
    });
  }
  static async products(req, res) {
    const errorMsg = req.flash('error');
    const infoMsg = req.flash('info');
    const user = getPayloadCookies(req);

    template(res, {
      page: 'products',
      user,
      props: {
        title: 'Products',
        info: infoMsg[0] ?? null, // Info message
        error: errorMsg[0] ?? null, // Error message
      },
    });
  }

  static async orderDetail(req, res) {
    const { id } = req.params;

    try {
      // Order data
      const order = await OrdersService.findOneRelProduct(id);
      // Payment data
      const { data } = await PaymentsService.getStatus(id);
      const { transaction_id, payment_type } = data;

      if (payment_type === 'qris' || payment_type === 'gopay') {
        // Get QR
        data['qr_image'] = PaymentsService.getQRImage(transaction_id, payment_type);
      } else {
        // Direct Debit
        if (payment_type !== 'bank_transfer' || payment_type !== 'credit_card') {
          // Get Payment URL
          data['payment_url'] = PaymentsService.getDirectDebitURL(transaction_id, payment_type);
        }
      }

      template(res, {
        page: 'orders/order-detail',
        props: {
          title: 'Order Detail | ' + data.id,
          data: order,
          payment: data,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  static async discount(req, res) {
    const user = getPayloadCookies(req);

    template(res, {
      page: 'discount',
      user,
      props: {
        title: 'Discounts',
      },
    });
  }
}

module.exports = ViewsController;
