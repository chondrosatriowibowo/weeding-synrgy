const { v4: uuidv4 } = require('uuid');
const PaymentService = require('../services/payment/payment.service');
const OrdersService = require('../services/orders.service');
const CheckoutsService = require('../services/checkouts.service');
const { getPayloadCookies } = require('../utils/jwt');

const { randomString } = require('../utils/generator');
const { responseHandler } = require('../utils/responseHandler');
const errMsg = require('../utils/errorMessage');

class OrdersController {
  /**
   * Find all orders realtion with product
   */
  static async findAllRelProduct(req, res, next) {
    try {
      const data = await OrdersService.findAllRelProduct();
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Find one multiple relations
   */
  static async findOneRelProduct(req, res, next) {
    const { id } = req.params;

    try {
      const data = await OrdersService.findOneRelProduct(id);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Checkout to get transaction token
   */
  static async checkout(req, res, next) {
    try {
      const user = getPayloadCookies(req);
      // Get checkout data by Client ID
      const { order_id, checkout_items } = await CheckoutsService.findLatestCheckoutUser(user.profileId);
      // Create payload for transaction
      const { gross_amount, item_details } = await OrdersService.getCheckoutPayload(JSON.parse(checkout_items));
      const transaction_details = { order_id, gross_amount };
      // Generate snap token
      const { token } = await PaymentService.generateTokenTransaction(
        { transaction_details, item_details }, user
      );
      return res.send({ token });
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
  /**
   * Create transaction to order after successfully checkout
   */
  static async onFinish(req, res, next) {
    // id for BCA klikpay
    // const { order_id, id } = req.query;

    try {
      req.flash('info', 'Payment success');
      return res.redirect('/order-list');
    } catch (error) {
      req.flash('error', 'Payment failed');
      return res.redirect('/order-list');
    }
  }

  /**
   * Notification hooks for updating Order
   */
  static async notificationHooks(req, res, next) {
    try {
      let { order_id, transaction_status, fraud_status } = req.body;
      const summary = `Transaction notification received. Order ID: ${order_id}. Transaction status: ${transaction_status}. Fraud status: ${fraud_status}.<br>Raw notification object:<pre>${JSON.stringify(
        transaction_status,
        null,
        2
      )}</pre>`;
      // Handle transaction status via notification
      if (transaction_status == 'capture') {
        if (fraud_status == 'challenge') {
          transaction_status = 'challenge';
        } else if (fraud_status == 'accept') {
          transaction_status = 'success';
        }
      } else if (transaction_status == 'cancel' || transaction_status == 'deny' || transaction_status == 'expire') {
        transaction_status = 'failure';
      }
      // Create Order transaction status based on Checkout order_id
      if (transaction_status === 'pending') {
        // Get Order data from Checkout
        const { ClientId, checkout_items } = await CheckoutsService.model.findOne({
          where: { order_id },
          raw: true,
        });
        // Create payload for transaction
        const payload = {
          payment_info: req.body,
          item_details: JSON.parse(checkout_items),
        };
        // Create Order
        await OrdersService.createTransactionOrder(payload, ClientId);
        // Find order_id in checkout
        const isOrderExist = await CheckoutsService.findOneWhereOptions({ order_id });
        if (isOrderExist) {
          // Clean checkout
          await CheckoutsService.updateWhereOptions({ order_id: null, checkout_items: null, order_data: null },
            { order_id }
          );
        }
      }
      // Update status Order
      else {
        await OrdersService.update(order_id, { status: transaction_status });
        // Update status in OrderProduct
        await OrdersService.updateProgress(order_id, undefined, transaction_status);
      }
      console.log(req.body);
      console.log(summary);
      return res.send(summary);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Update progress status in OrderProduct for Vendor
   */
  static async updateProgress(req, res, next) {
    const { redirect, OrderId, ProductId, type } = req.query;
    const { progress } = req.body;

    try {
      const data = await OrdersService.updateProgress(OrderId, ProductId, progress, type);
      return redirect ? res.redirect('/') : responseHandler(res, 200, data);
    } catch (error) {
      return redirect ? res.redirect('/') : next(errMsg(400, error, error.message));
    }
  }

  static async statusPayment(req, res, next) {
    const { redirect } = req.query;
    const { id } = req.params;

    try {
      const { data } = await PaymentService.getStatus(id);
      return redirect ? res.redirect('/') : responseHandler(res, 200, data);
    } catch (error) {
      return redirect ? res.redirect('/') : next(errMsg(400, error, error.message));
    }
  }

  static async updateStatusPayment(req, res, next) {
    const { id } = req.params;
    const { status } = req.query;

    try {
      const { data } = await PaymentService.updateStatus(id, status);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  static unfinishHooks(req, res, next) {
    console.log(req.query);
    res.send('unfinish',req.body);
  }

  static async findAllOrderProductUser(req, res, next) {
    let { isSession, ClientId } = req.query;
    isSession = isSession !== 'false';

    try {
      const { profileId, roles } = getPayloadCookies(req);
      // Check if session is not a CLIENT
      if (isSession && roles !== 'CLIENT') throw new Error('You are not a client');
      // Client session or not
      const id = isSession ? profileId : ClientId;
      const data = await OrdersService.findAllOrderProductByUserId(id, req.query);
      return responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }
}

module.exports = OrdersController;
