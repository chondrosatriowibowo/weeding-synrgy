const viewsController = require('./views.controller');
const accountController = require('./accounts.controller');
const authController = require('./auth.controller');
const clientController = require('./clients.controller');
const vendorController = require('./vendors.controller');
const productController = require('./product.controller');
const orderController = require('./orders.controller');
const feedbackController = require('./feedback.controller');
const promoController = require('./promo.controller');
const checkoutController = require('./checkouts.controller');


module.exports = {
  viewsController,
  accountController,
  authController,
  clientController,
  vendorController,
  productController,
  orderController,
  feedbackController,
  promoController,
  checkoutController,
};
