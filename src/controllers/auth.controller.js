const COOKIE_CONFIG = require('../config/cookies');

const AccountsService = require('../services/accounts.service');
const AccountProvidersService = require('../services/accountProviders.service');
const { AuthService } = require('../services/firebase/firebase.service');
const EmailService = require('../services/email/email.service');

const { responseHandler } = require('../utils/responseHandler');
const jwt = require('../utils/jwt');
const errMsg = require('../utils/errorMessage');
const { randomString } = require('../utils/generator');

class AuthController {
  static setCookiesProvider(res, payload, avatar) {
    // Sign JWT
    const token = jwt.sign({
      ...payload,
      phone: payload.phone,
      avatar,
    });
    // Set Cookies
    res.cookie('_acct', token, COOKIE_CONFIG);
    return token;
  }
  /**
   * Register account with email & password
   */
  static async register(req, res, next) {
    const { role, redirect } = req.query;
    const { avatar } = req.files;

    try {
      // User can't register using email password, if already registered using Google
      const providerEmail = await AccountProvidersService.getEmail(req.body.email);
      // If email provider is exist
      if (providerEmail) {
        throw new Error('Please login with Google');
      }
      // Parse categories to array if Vendor
      if (role === 'vendor') {
        req.body.categories = JSON.parse(req.body.categories);
      }
      const data = await AccountsService.createRegister(role, req.body, avatar ? avatar[0] : null);
      // Send email verification
      data.emailStatus = await EmailService.sendMailVerification('verification', data);
      req.flash('info', 'Please check your email to verify your account');
      return redirect ? res.redirect('/') : responseHandler(res, 200, data);
    } catch (error) {
      next(errMsg(400, error, error.message));
    }
  }

  /**
   * Provider handler (Login or Register)
   * @param {'LOGIN' | 'REGISTER'} strategy
   * @returns
   */
  static providerHandler = (strategy) => async (req, res, next) => {
    // Check if query redirect is exist
    const { redirect } = req.query;

    try {
      let account;
      // Token from google sign in
      const g_token = await AuthService.signInWithGoogle(req.body.credential);
      // Account provider from database
      account = await AccountProvidersService.findOneAuth(g_token.email);
      // Check if it's login or register
      if (strategy === 'LOGIN') {
        if (!account) {
          throw new Error('Account not found');
        }
      } else if (strategy === 'REGISTER') {
        if (!account) {
          // Get account and client data by email provider
          const user = await AccountsService.findOneAuthProvider(g_token.email);
          if(user) {
            // Do transaction to create Account Provider and Update Account
            account = await AccountProvidersService.connectToAccount(user.profileId, g_token);
            account['id'] = user.accountId;
            account['name'] = user.name;
            account['phone'] = user.phone;
          } else {
            account = await AccountProvidersService.createRegister(g_token);
            delete account['AccountProviderId'];
            delete account['AccountId'];
          }
        }
      }
      console.log(account);
      account['strategy'] = 'PROVIDER';
      // Set Cookies
      const token = this.setCookiesProvider(res, account, g_token.photoURL);

      return redirect ? res.redirect('/') : responseHandler(res, 200, { token });
    } catch (error) {
      console.log('errnya', error);
      const endpoint = strategy === 'LOGIN' ? '/login' : '/register';
      req.flash('error', error.message);
      return redirect ? res.redirect(endpoint) : next(errMsg(400, error, error.message));
    }
  };

  /**
   * Register with provider (Google)
   */
  static registerProvider = this.providerHandler('REGISTER');

  /**
   * Login with provider (Google)
   */
  static loginProvider = this.providerHandler('LOGIN');

  /**
   * Login with email & password
   */
  static login(req, res, next) {
    // Check if query redirect is exist
    const { redirect } = req.query;
    // Sign JWT
    const token = jwt.sign(req.user);
    // Set Cookies
    res.cookie('_acct', token, COOKIE_CONFIG);
    return redirect ? res.redirect('/') : responseHandler(res, 200, { token });
  }

  static logout(req, res, next) {
    const { redirect } = req.query;
    res.clearCookie('_acct', { path: COOKIE_CONFIG.path });
    return redirect ? res.redirect('/') : responseHandler(res, 200, { message: 'Logout success' });
  }

  static checkPayload(req, res, next) {
    return responseHandler(res, 200, { token: jwt.getPayloadCookies(req) });
  }

  /**
   * Account verification with email
   */
  static async accountVerification(req, res, next) {
    const { verifyCode } = req.params;

    try {
      // Find verify code
      const valid = await AccountsService.updateWhereOptions({ isVerified: true }, { verifyCode });
      // Verify code invalid
      if (valid[0] === 0) throw new Error('Verify code not found');
      return res.render('pages/verification', { success: true });
    } catch (error) {
      return res.render('pages/verification', { success: false });
    }
  }

  /**
   * Forgot password
   */
  static async forgotPassword(req, res, next) {
    const { redirect } = req.query;
    const { email } = req.body;

    try {
      // Find account by email
      const data = await AccountsService.findOneAuth(email);
      const verifyCode = randomString(50);
      // Generate new token if account status is verified
      if(data.isVerified) {
        await AccountsService.updateWhereOptions({ verifyCode }, { email });
      }
      // Send email verification
      data.emailStatus = await EmailService.sendMailVerification('forgotPassword',
        data.isVerified ? { ...data, verifyCode } : data
      );
      req.flash('info', 'Please check your email to reset your account');
      return  responseHandler(res, 200, data);
    } catch (error) {
      req.flash('error', 'Email not found');
      return  next(errMsg(400, error, error.message));
    }
  }
}

module.exports = AuthController;
