const { Account, Admin, Client, Vendor, sequelize, Feedback, Promo } = require('../models');
const Service = require('./service');
const {randomString} = require('../utils/generator')

class PromoService extends Service{
    /**
   * Create product
   * @returns {Promise<any>} all data
   */
     static async createPromo(params, payload) {
        try {
            const {productId} = params 
            let randomPromoCode = randomString(Number(10))
            let data = {
              ...payload,
              ProductId: productId,
              promoCode: randomPromoCode,
            }

            return await this.model.create(data)        
          } 
         catch (error) {
            throw error
        }
      }
}
PromoService.model = Promo;
  
module.exports = PromoService;