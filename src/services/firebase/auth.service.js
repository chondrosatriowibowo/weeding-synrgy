const { FirebaseApp } = require('firebase/app');
const { getAuth, signInWithCredential, UserInfo, GoogleAuthProvider } = require('firebase/auth');

class FirebaseAuth {
  /**
   * Firebase app instance
   * @param {FirebaseApp} firebase - Firebase app instance
   */
  constructor(firebase) {
    this.firebase = firebase;
  }

  /**
   *  Sign in with google provider
   * @param {String} id_token - JWT token
   * @returns {Promise<UserInfo>} - User info
   */
  async signInWithGoogle(id_token) {
    try {
      // Create credential with the Google ID token.
      const credential = GoogleAuthProvider.credential(id_token);
      // Auth instance
      const authApp = getAuth(this.firebase);
      // Sign in with credential
      const auth = await signInWithCredential(authApp, credential);
      // The signed-in user info.
      return auth.user.providerData[0];
    } catch (error) {
      throw error;
    }
  }
}

module.exports = FirebaseAuth;
