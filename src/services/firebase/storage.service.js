const { FirebaseApp } = require('firebase/app');
const Compat = require('firebase/compat/app').default;
require('firebase/compat/storage');
const { getStorage, ref, uploadBytes, getDownloadURL, deleteObject } = require('firebase/storage');
const { mimeTypeCheck } = require('../../utils/fileHandler');

class FirebaseStorage {
  /**
   * Firebase app instance
   * @param {FirebaseApp} firebase - Firebase app instance
   * @param {Compat.app.App} firebaseCompat - Firebase old version app instance
   */
  constructor(firebase, firebaseCompat) {
    this.firebase = firebase;
    this.firebaseCompat = firebaseCompat;
    this.storage = getStorage(firebase);
  }

  /**
   * Upload file
   * @param {'products' | 'profile' | 'purchases' | 'vendors'} path - Folder path in firebase storage
   * @param {any} file - Object files from multer
   * @param {'pdf' | 'image'} targetType - Target MIME type of uploaded file
   * @returns {Promise<String>} Download URL
   */
  async upload(path, file, targetType) {
    try {
      // MIME type check
      mimeTypeCheck(file.mimetype, targetType);
      // Custom file name and path folder in storage
      const fileName = `${path}/${file.fieldname}-${Date.now()}${file.originalname}`;
      // Create storage reference
      const storageRef = ref(this.storage, fileName);
      // Do upload byte file into firebase storage
      const uploadFile = await uploadBytes(storageRef, file.buffer, {
        contentType: file.mimetype,
      });
      // Get download URL after upload
      const downloadURL = await getDownloadURL(uploadFile.ref);
      return downloadURL;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Delete file
   * @param {String} url - Download URL
   * @returns
   */
  async delete(url) {
    try {
      // Create ref by URL
      const storageRef = this.firebaseCompat.storage().refFromURL(url);
      // Delete file from storage
      await deleteObject(storageRef);
      // Return value
      return true;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = FirebaseStorage;
