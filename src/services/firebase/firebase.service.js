const config = require('../../config/firebase');
const { initializeApp } = require('firebase/app');
const firebaseCompat = require('firebase/compat/app').default;
const Storage = require('./storage.service');
const Auth = require('./auth.service');
/**
 * Initialize App
 */
// Firebase app
const app = initializeApp(config);
// Firebase app compat
const appCompat = firebaseCompat.initializeApp(config);
// Storage service instance
const StorageService = new Storage(app, appCompat);
// Auth service instance
const AuthService = new Auth(app);

module.exports = {
  StorageService,
  AuthService,
}
