const { Checkout, Client, Account, Order, OrderProduct, Product, Vendor, Sequelize } = require('../models');
const { Op } = Sequelize;
const Service = require('./service');
const { v4: uuidv4 } = require('uuid');

class CheckoutsService extends Service {
  /**
   * Check if Client has checkout or not
   * @param {string} ClientId - Client ID
   */
  static async isClientExist(ClientId) {
    const data = await this.model.findOne({
      where: { ClientId },
      raw: true,
    });
    if (data) return true;
    return false;
  }

  /**
   * Format detail items for midtrans request
   * @param {Array<{id: string, book_date: string, quantity: number}>} items - items from Checkout
   * @returns {Promise<Array<{id: string, name: string, book_date: string, price: number, sub_total: number, discount: number, quantity: number}>>}
   */
  static async formatDetailItems(items) {
    try {
      // Get product ID property
      const productId = items.map((obj) => obj.id);
      // Find name and price by array of ID
      const data = await Product.findAll({
        attributes: ['id', 'name', 'price', 'discount'],
        where: { id: { [Op.in]: productId } },
        raw: true,
      });
      // Calculate sub total
      const getElementLocal = (id, el) => items.find((obj) => obj.id === id)[el];
      // Check if discount is exist
      const isDiscount = (discount, price) => {
        if (!discount || discount === 0) return price;
        return parseInt(price - price * (discount / 100));
      };
      for (const el of data) {
        el.quantity = getElementLocal(el.id, 'quantity');
        el.book_date = getElementLocal(el.id, 'book_date');
        el.price = isDiscount(el.discount, el.price); // Update current price with discount if exist
        el.sub_total = el.price * getElementLocal(el.id, 'quantity');
      }
      return data;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Find all checkout by Client ID and returns product detail
   */
  static async findAllRelProducts(ClientId) {
    try {
      const { checkout_items } = await this.model.findOne({
        attributes: ['checkout_items'],
        where: { ClientId },
        raw: true,
        order: [['id', 'DESC']],
      });
      // Get product ID property
      const productId = JSON.parse(checkout_items).map((obj) => obj.id);
      // Return Products
      const data = await Product.findAll({
        where: { id: { [Op.in]: productId } },
        raw: true,
      });
      return data;
    } catch (error) {
      return [];
    }
  }

  /**
   * Get user checkout items categorized by vendor
   * @param {string} ClientId - Client ID
   */
  static async findCheckoutItems(ClientId) {
    try {
      const { checkout_items } = await this.model.findOne({
        attributes: ['checkout_items'],
        where: { ClientId },
        raw: true,
        order: [['id', 'DESC']],
      });
      // Get product ID property
      const productId = JSON.parse(checkout_items).map((obj) => obj.id);
      const data = await Vendor.findAll({
        attributes: ['id', 'name', 'categories', 'isVerified'],
        include: [
          {
            model: Product,
            attributes: ['id', 'name', 'price', 'images', 'discount'],
            include: [
              {
                model: OrderProduct,
              }
            ],
            where: { id: { [Op.in]: productId } },
          },
        ],
      });

      return data;
    } catch (error) {
      return [];
    }
  }

  /**
   * Create or update checkout, based on Client ID
   * @param {string} ClientId - Client ID
   * @param {Array<{id: string, quantity: number}>} checkout_items - items of ID
   * @param {{book_date: string}} order_data - Order data
   * @returns {Promise<any>}
   */
  static async createCheckout(ClientId, checkout_items, order_data) {
    try {
      const order_id = uuidv4();
      // Stringify data
      checkout_items = JSON.stringify(await this.formatDetailItems(checkout_items));
      // Create new checkout
      await this.create({ ClientId, order_id, checkout_items });
      return { status: 'success' };
    } catch (error) {
      throw new Error('Failed to create checkout');
    }
  }

  /**
   * Find all latest checkout by Client ID
   * @param {string} ClientId - Client ID from cookies payload
   */
  static async findLatestCheckoutUser(ClientId) {
    const data = await this.model.findOne({
      where: { ClientId },
      order: [['id', 'DESC']],
      raw: true,
    });
    return data;
  }
}
CheckoutsService.model = Checkout;

module.exports = CheckoutsService;
