class Service {
  /**
   * Sequelize Model
   */
  static model;
  static offset = 0;
  static limit = 20;

  /**
   * Match Payload
   * @param {any} modelRole - sequelize model
   * @param {any} payload
   * @returns
   */
  static async matchPayload(modelRole, payload) {
    return new Promise((resolve, reject) => {
      let obj = {};
      const keys = Object.keys(modelRole.getAttributes());
      if (!payload) reject('Request body is empty');

      keys.forEach((key) => {
        obj[key] = payload[key] ?? null;
      });

      resolve(obj);
    });
  }

  /**
   * Create Query
   * @param {any} payload
   * @returns {Promise<any>} inserted data
   */
  static async create(payload) {
    return await this.model.create(payload);
  }

  /**
   * Find All Query
   * @returns {Promise<any>} all data
   */
  static async findAll() {
    return await this.model.findAll();
  }

  /**
   * Find All Pagination
   * @param {number} offset - Current page
   * @param {number} limit - Limit of data per page
   * @param {Object} options - Custom options for find all query
   */
  static async findAllPagination(offset=this.offset, limit=this.limit, options) {
    return await this.model.findAndCountAll({
      offset,
      limit,
      ...options,
    });
  }
  static async findOnePagination(offset=this.offset, limit=this.limit, options) {
    return await this.model.findOne({
      offset,
      limit,
      ...options,
    });
  }

  /**
   * Calculate Total Page
   * @param {number} count - Total count data
   * @param {number} limit - Total data per page
   */
  static totalPage(count, limit=this.limit) {
    let result = count / limit;
    if(count % limit !== 0) return parseInt(result) + 1;
    return result;
  }

  /**
   * Find All Query Custom Where Options
   * @returns {Promise<any>} all data
   */
  static async findAllWhereOptions(options) {
    return await this.model.findAll({ where: options });
  }

  /**
   * Find All Query Custom Multi Where Options
   * @returns {Promise<any>} all data
   */
  static async findAllMultiWhereOptions(params) {
    return await this.model.findAll({ where: { ...params } });
  }

  /**
   * Find One Query
   * @param {string} id
   * @returns {Promise<any>} get one data
   */
  static async findOne(id) {
    return await this.model.findOne({ where: { id } });
  }

  /**
   * Find one custom options where
   * @returns {Promise<any>}
   */
  static async findOneWhereOptions(options) {
    return await this.model.findOne({ where: options });
  }

  /**
   * Update Query
   * @param {string} id
   * @param {any} payload
   * @returns {Promise<any>}
   */
  static async update(id, payload) {
    return await this.model.update(payload, { where: { id } });
  }

  /**
   * Update Query Custom Where
   * @param {any} payload
   * @param {any} options
   * @returns {Promise<any>}
   */
  static async updateWhereOptions(payload, options) {
    return await this.model.update(payload, { where: options });
  }

  /**
   * Remove Query
   * @param {string} id
   * @returns {Promise<any>}
   */
  static async remove(id) {
    return await this.model.destroy({ where: { id } });
  }

  /**
   * Remove Query
   * @param {Object} options
   * @returns {Promise<any>}
   */
  static async removeWhereOptions(options) {
    return await this.model.destroy({ where: options });
  }

  /**
   * Find One Query to Get Email
   * @param {string} email - email in Accounts or Account Providers
   * @returns {Promise<any>} get one data
   */
  static async getEmail(email) {
    return await this.model.findOne({ attributes: ['email'], where: { email } });
  }
}

module.exports = Service;
