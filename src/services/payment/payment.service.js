const axios = require('axios').default;
const { authorization, endpoint } = require('../../config/midtrans');
const { randomString } = require('../../utils/generator');

class PaymentService {
  constructor() {
    /**
     * Create instance axios
     * @param {'v1' | 'v2'} version
     */
    const instance = (version) =>
      axios.create({
        baseURL: version === 'v1' ? endpoint.v1 : endpoint.v2,
        headers: {
          Accept: 'application/json',
          Authorization: 'Basic ' + authorization,
          'Content-Type': 'application/json',
        },
      });
    /**
     * Midtrans API v1 (Snap)
     */
    this.v1 = instance('v1');
    /**
     * Midtrans API v2 (Core API)
     */
    this.v2 = instance('v2');
  }

  /**
   * Generate new token transaction snap
   * @param {{
   * transaction_details: {order_id: string, gross_amount: number},
   * item_details: Array.<{id: string, name: string, price: number, quantity: number}>
   * }} data
   * @param {{id: string, name: string, email: string, phone: string}} user
   */
  async generateTokenTransaction(data, user) {
    try {
      // Define customer information
      const customer_details = {
        first_name: user.name,
        email: user.email,
        phone: user.phone,
      };
      // Get token
      const token = await this.v1.post('/transactions', { ...data, customer_details, user_id: user.id });
      return token.data;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Get status transaction or order by ID
   * @param {string} orderId
   * @returns
   */
  async getStatus(orderId) {
    try {
      // Get status
      const status = await this.v2.get(orderId + '/status');
      return status;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Simple charge test
   */
  async charge() {
    try {
      const data = {
        payment_type: 'bank_transfer',
        bank_transfer: {
          bank: 'permata',
        },
        transaction_details: {
          order_id: randomString(10),
          gross_amount: 1000,
        },
      };
      const result = await this.v2.post('/charge', data);
      return result;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Update status payment
   * @param {string} orderId - Order ID or Transaction ID
   * @param {'approve' | 'deny' | 'cancel' |
   * 'expire' | 'refund' | 'direct_refund'} status - Status you want to set for order
   */
  async updateStatus(orderId, status) {
    try {
      // Based on status param
      const method = status === 'direct_refund' ? 'refund/online/direct' : status;
      // Get status
      const result = await this.v2.post(orderId + '/' + method);
      return result;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Get QR Image
   * @param {string} transactionId - Transaction ID
   * @param {'gopay' | 'qris'} channel - Channel
   */
  getQRImage(transactionId, channel) {
    // https://api.sandbox.midtrans.com/v2/qris//qr-code
    transactionId = channel === 'qris' ? `shopeepay/sppq_${transactionId}` : transactionId;
    return `${endpoint.v2}/${channel}/${transactionId}/qr-code`;
  }

  /**
   * Get Direct Debit URL
   * @param {string} transactionId - Transaction ID
   * @param {'bca_klikpay' | 'cimb_clicks' | 'danamon_online' | 'akulaku'} channel - Channel
   */
  getDirectDebitURL(transactionId, channel) {
    // https://api.sandbox.midtrans.com/v3/bca/klikpay/redirect/925ec068-a30c-4141-bb81-8b2451dbd909
    // https://api.sandbox.midtrans.com/cimb-clicks/request?id=11b8fd57-f7f2-4bd7-ad6d-5273b480443e
    // https://api.sandbox.midtrans.com/v2/danamon/online/redirect/f8756969-de4a-418e-98a8-386f607ac052
    // https://api.sandbox.midtrans.com/v2/akulaku/redirect/69cb8141-7d41-402b-9d9c-ed8caf4a121d
    let api;
    switch (channel) {
      case 'bca_klikpay':
        api = endpoint.v3;
        channel = 'bca/klikpay/redirect/' + transactionId;
        break;
      case 'cimb_clicks':
        api = endpoint.default;
        channel = 'cimb-clicks/request?id=' + transactionId;
        break;
      case 'danamon_online':
        api = endpoint.v2;
        channel = 'danamon/online/redirect/' + transactionId;
        break;
      case 'akulaku':
        api = endpoint.v2;
        channel = 'akulaku/redirect/' + transactionId;
        break;
      default:
        break;
    }

    return `${api}/${channel}`;
  }
}
module.exports = new PaymentService();
