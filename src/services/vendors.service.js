const { Vendor, Account, Product, Feedback, Sequelize, sequelize } = require('../models');
const { Op } = Sequelize;
const Service = require('./service');
const { StorageService } = require('./firebase/firebase.service');

class VendorsService extends Service {
  static categories = ['Fotografi', 'Dekorasi', 'Musik', 'Katering'];
  /**
   * Find all vendor relations with account
   * @returns {Promise<any>}
   */
  static async findAllRelAccount() {
    return await this.model.findAll({
      include: [
        {
          model: Account,
          attributes: ['email', 'roles'],
        },
      ],
      raw: true,
    });
  }

  /**
   * Find one vendor relations with account
   * @returns {Promise<any>}
   */
  static async findOneRelAccount(id) {
    return await this.model.findOne({
      include: [
        {
          model: Account,
          attributes: ['email', 'roles'],
        },
      ],
      where: { id },
      raw: true,
    });
  }

  /**
   * Detail Vendor Head Section
   * @param {string} id - Vendor ID
   */
  static async detailVendorHead(id) {
    return await this.model.findOne({
      attributes: ['id', 'url', 'name', 'categories', 'address', 'city', 'phone', 'isBanned', 'isVerified',
        [sequelize.fn('avg', sequelize.col('Products.Feedbacks.rating')), 'starOutOf'],
        [sequelize.fn('count', sequelize.col('Products.Feedbacks.id')), 'totalReview'],
      ],
      include: [
        {
          model: Account,
          attributes: ['id', 'email'],
        },
        {
          model: Product,
          attributes: [],
          include: [
            {
              model: Feedback,
              attributes: [],
            },
          ],
        },
      ],
      where: { id },
      group: ['Vendor.id', 'Account.id'],
    });
  }

  /**
   * Find All Query Custom Multi Where Options
   * @param {number} offset
   * @param {number} limit
   * @returns {Promise<any>} all data
   */
  static async findAllMultiWhereOptions(offset, limit, params) {
    let { categories, price, search } = params;
    let query = params,
      q_search,
      res_count,
      q_price = [sequelize.col('minPrice'), 'ASC'];
    // Delete props from database query
    delete query.search;
    delete query.price;
    delete query.offset;
    delete query.limit;

    if (categories) {
      // Parse to array if type categories is a string
      if (typeof categories === 'string') {
        categories = [categories];
      }
      // Find matching array
      query.categories = {
        [Op.contains]: categories,
      };
    }
    // Price sort ASC or DESC
    if (price) {
      if (price === 'max') {
        q_price = [sequelize.col('maxPrice'), 'DESC'];
      }
    }
    if (search) {
      const q_like = { [Op.iLike]: `%${[search]}%` };
      q_search = { [Op.or]: [{ name: q_like }, { city: q_like }] };
    }
    const count = (isFilter = false) => this.model.count(isFilter ? { where: { ...query, ...q_search } } : {});
    const data = await this.model.findAll({
      offset: offset ?? this.offset,
      limit: limit ?? this.limit,
      attributes: ['id', 'url', 'name', 'categories', 'banner', 'address', 'city', 'isBanned', 'isVerified',
        [sequelize.fn('min', sequelize.col('Products.price')), 'minPrice'],
        [sequelize.fn('max', sequelize.col('Products.price')), 'maxPrice'],
        [sequelize.fn('avg', sequelize.col('Products.Feedbacks.rating')), 'starOutOf'],
        [sequelize.fn('count', sequelize.col('Products.Feedbacks.id')), 'totalReview'],
      ],
      include: [
        {
          model: Product,
          attributes: [],
          include: [
            {
              model: Feedback,
              attributes: [],
            },
          ],
        },
      ],
      subQuery: false,
      where: { ...query, ...q_search },
      order: [q_price],
      group: ['Vendor.id'],
    });
    // Show count with filter where clause or not
    if (!query) res_count = !q_search ? false : true;
    else res_count = true;
    // If filter or search is exist, count data with WHERE clause
    return { count: await count(res_count), rows: data };
  }

  /**
   * Update Query
   * @param {string} id
   * @param {any} payload
   * @param {any} file
   * @returns {Promise<any>}
   */
  static async update(id, payload, files) {
    try {
      const docFile = files.document ? files.document[0] : null;
      const imgFile = files.banner ? files.banner[0] : null;

      console.log(files);

      const data = await sequelize.transaction(async (t) => {
        // Get current document URL
        const { document, banner } = await this.findOne(id);
        // Update vendor check constraint
        const updateVendor = await this.model.update(payload, { where: { id }, transaction: t });
        // Update document
        const updateDocument = async () => {
          // Check if file and document is exist
          if (imgFile) {
            var bannerURL = await StorageService.upload('vendors', imgFile, 'image');
            if (banner && banner != '/images/banner.png') {
              await StorageService.delete(banner);
            }
            await this.model.update({ banner: bannerURL }, { where: { id }, transaction: t });
          }
          if (docFile) {
            var documentURL = await StorageService.upload('vendors', docFile, 'pdf');
            // Document is exist
            if (document) {
              // Upload file
              await StorageService.delete(document);
            }
            // Update data vendor
            await this.model.update({ document: documentURL }, { where: { id }, transaction: t });
            return;
          }
          return;
        };
        await updateDocument();
        return updateVendor;
      });

      return data;
    } catch (error) {
      throw error;
    }
  }
}
VendorsService.model = Vendor;

module.exports = VendorsService;
