const { Client, Account, AccountProvider, Sequelize } = require('../models');
const { Op } = Sequelize;
const Service = require('./service');

class ClientsService extends Service {
  /**
   * Find all client relations with account
   * @returns {Promise<any>}
   */
  static async findAllRelAccount() {
    return await this.model.findAll({
      include: [
        {
          model: Account,
          attributes: ['email', 'roles'],
        },
      ],
      raw: true,
    });
  }

  /**
   * Find one Client relations with Account
   * @param {string} id - Client ID
   * @returns {Promise<any>}
   */
  static async findOneRelAccount(id) {
    return await this.model.findOne({
      include: [
        {
          model: Account,
          attributes: ['email', 'roles'],
        },
        {
          model: AccountProvider,
          attributes: ['email', 'roles'],
        },
      ],
      where: { id },
      raw: true,
    });
  }

  /**
   * Check if Client has provider account or not
   * @param {string} id - could be from Account or AccountProvider
   */
  static async isProviderExist(id) {
    return await this.model.findOne({
      attributes: ['AccountProviderId'],
      include: [
        {
          model: Account,
          attributes: ['email'],
        },
      ],
      raw: true,
      where: { [Op.or]: [{ AccountId: id }, { AccountProviderId: id }] },
    });
  }
}
ClientsService.model = Client;

module.exports = ClientsService;
