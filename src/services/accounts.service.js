const { Account, Admin, Client, Vendor, sequelize } = require('../models');
const Service = require('./service');
const ClientsService = require('./clients.service');
const { StorageService } = require('./firebase/firebase.service');
const bcrypt = require('../utils/bcrypt');

class AccountsService extends Service {
  /**
   * Register account
   * @param {'client' | 'vendor' | 'admin'} roles
   * @param {*} payload
   * @param {*} file - avatar image
   * @returns {Promise<any>}
   */
  static async createRegister(roles = 'client', payload, file) {
    const modelRole = roles === 'client' ? Client : Vendor;

    try {
      // Transaction for creating account
      const data = await sequelize.transaction(async (t) => {
        const hash = await bcrypt.hash(payload.password);
        // Data Account
        const dataAccount = {
          roles: roles === 'client' ? 'CLIENT' : 'VENDOR',
          email: payload.email,
          password: hash,
        };
        // Do create query for Account
        const accountCreate = await Account.create(dataAccount, { transaction: t });
        // Do create query for People
        const peopleCreate = await modelRole.create(
          {
            ...payload,
            AccountId: accountCreate.id, // Set AccountId for People (Client or Vendor)
          },
          { transaction: t }
        );
        // Update avatar account after transaction
        const updateAvatar = async () => {
          if (file) {
            const avatarURL = await StorageService.upload('profile', file, 'image');
            await Account.update({ avatar: avatarURL }, { where: { id: accountCreate.id }, transaction: t });
            return avatarURL;
          }
          return;
        };
        // Return data
        const result = { ...accountCreate.dataValues, ...peopleCreate.dataValues };
        // Check if file exist or not
        return file ? { ...result, avatar: await updateAvatar() } : result;
      });
      return data;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Find account by email for authentication
   * @param {string} email
   * @returns {Promise<any>}
   */
  static async findOneAuth(email) {
    try {
      const data = await sequelize.transaction(async (t) => {
          const account = await this.model.findOne(
            {
              attributes: ['id', 'roles', 'isVerified', 'verifyCode', 'email', 'avatar', 'password'],
              where: { email },
            },
            { transaction: t }
          );
          // Client or Vendor model
          const modelProfile = account.roles === 'CLIENT' ? Client : Vendor;
          // Model of Client | Vendor | Admin
          const modelRole = account.roles === 'ADMIN' ? Admin : modelProfile;
          // Find one profile
          const profile = await modelRole.findOne(
            {
              attributes: account.roles === 'ADMIN' ? ['name'] : ['id', 'name', 'phone'],
              where: { AccountId: account.id },
            },
            { transaction: t }
          );

          return { ...account.dataValues, ...profile.dataValues,
            id: account.dataValues.id, profileId: profile.dataValues.id };
        }) ?? null;
      return data;
    } catch (error) {
      throw Error('Account not found');
    }
  }

  /**
   * Find account by email for authentication provider
   * @param {string} email
   * @returns {Promise<any>}
   */
  static async findOneAuthProvider(email) {
    try {
      const data =
        (await sequelize.transaction(async (t) => {
          const account = await this.model.findOne(
            {
              attributes: ['id', 'email', 'roles'],
              where: { email },
            },
            { transaction: t }
          );
          // Client or Vendor model
          const modelProfile = account.roles === 'CLIENT' ? Client : Vendor;
          // Model of Client | Vendor | Admin
          const modelRole = account.roles === 'ADMIN' ? Admin : modelProfile;
          // Find one profile
          const profile = await modelRole.findOne(
            {
              attributes: account.roles === 'ADMIN' ? ['name'] : ['id', 'name', 'phone'],
              where: { AccountId: account.id },
            },
            { transaction: t }
          );

          return { ...account.dataValues, ...profile.dataValues,
            accountId: account.dataValues.id, profileId: profile.dataValues.id
           };
        })) ?? null;
      return data;
    } catch (error) {
      return null;
    }
  }

  /**
   * Update Account
   * @param {string} accountEmail - Email from Accounts
   * @param {Object} session - Payload from cookies
   * @param {any} payload
   * @returns
   */
  static async update(accountEmail, session, payload) {
    try {
      const isProviderExist = await ClientsService.isProviderExist(session.id);
      // Do update query for Client
      if (payload.name || payload.phone) {
        await Client.update({ name: payload.name, phone: payload.phone }, { where: { id: session.profileId } });
      }
      // Do update query for Account or AccountProvider
      const data = await sequelize.transaction(async (t) => {
        let result;
        // Client or Vendor model
        const modelProfile = session.roles === 'CLIENT' ? Client : Vendor;
        // Model of Client | Vendor | Admin
        const modelRole = session.roles === 'ADMIN' ? Admin : modelProfile;
        // Hash password
        if(payload.password === '' || !payload.password) delete payload.password;
        else payload['password'] = await bcrypt.hash(payload.password);
        // Check if provider exist or not
        // Case: User have a different email between Account and AccountProvider
        if (isProviderExist && isProviderExist['Account.email'] !== accountEmail) {
          return await this.model.update(payload, { where: { email: isProviderExist['Account.email'] }, transaction: t });
        }
        // Case: User already have email password account and wants to update account info
        if (accountEmail) {
          result = await this.model.update(payload, { where: { email: accountEmail }, transaction: t });
        } else {
          // Do create account
          // Case: User doesn't have email password account and wants to setup email password account
          payload['isVerified'] = true;
          payload['email'] = session.email;
          payload['roles'] = session.roles;
          payload['avatar'] = session.avatar;
          payload['phone'] = session.phone;
          // Create Account
          const createAccount = await this.model.create(payload, { transaction: t });
          // Update AccountId for People (Client or Vendor)
          result = await modelRole.update(
            { AccountId: createAccount.id },
            { where: { AccountProviderId: session.id }, transaction: t }
          );
        }
        return result;
      });

      return data;
    } catch (error) {
      throw error;
    }
  }
}
AccountsService.model = Account;

module.exports = AccountsService;
