const { AccountProvider, Client, Vendor, Sequelize, sequelize } = require('../models');
const { Op } = Sequelize;
const { UserInfo } = require('firebase/auth');
const Service = require('./service');

class AccountProvidersService extends Service {
  /**
   * Register account with provider
   * @param {UserInfo} payload - User payload from google sign in
   * @returns {Promise<any>}
   */
  static async createRegister(payload) {
    try {
      // Transaction for creating account
      const data = await sequelize.transaction(async (t) => {
        // Data Account Provider
        const dataAccount = {
          roles: 'CLIENT',
          email: payload.email,
          avatar: payload.photoURL,
        };
        // Do create query for Account Provider
        const accountProviderCreate = await this.model.create(dataAccount, { transaction: t });
        // Data Client
        const dataClient = {
          AccountProviderId: accountProviderCreate.id,
          name: payload.displayName,
          phone: payload.phoneNumber,
        };
        // Delete key phone if null
        if (!payload.phoneNumber) delete dataClient.phone;
        // Do create query for Client
        const clientCreate = await Client.create(dataClient, { transaction: t });

        return { ...accountProviderCreate.dataValues, ...clientCreate.dataValues,
            id: accountProviderCreate.dataValues.id, profileId: clientCreate.dataValues.id
           };
      });
      return data;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Find account by email for authentication
   * @param {string} email
   * @returns {Promise<any>}
   */
  static async findOneAuth(email) {
    try {
      const data =
        (await sequelize.transaction(async (t) => {
          const accountProvider = await this.model.findOne(
            {
              attributes: ['id', 'roles', 'email'],
              where: { email },
            },
            { transaction: t }
          );
          // Client or Vendor model
          const modelRole = accountProvider.roles === 'CLIENT' ? Client : Vendor;
          // Find one profile
          const profile = await modelRole.findOne(
            {
              attributes: ['id', 'name', 'phone'],
              where: { AccountProviderId: accountProvider.id },
            },
            { transaction: t }
          );

          return { ...accountProvider.dataValues, ...profile.dataValues,
            id: accountProvider.dataValues.id, profileId: profile.dataValues.id };
        })) ?? null;

      return data;
    } catch (error) {
      return null;
    }
  }

  /**
   * Check if current user is connected to provider or not
   * @param {Object} user - Payload from cookies
   */
  static async isConnectedProvider(user) {
    const { id, strategy, roles } = user;

    if (strategy === 'LOCAL') {
      // For current, vendor not using Account Provider
      // const modelRole = roles === 'CLIENT' ? Client : Vendor;
      if (roles === 'VENDOR') {
        return false;
      }
      const { AccountProviderId } = await Client.findOne({
        attributes: ['AccountProviderId'],
        where: { [Op.or]: [{ AccountId: id }, { AccountProviderId: id }] },
      });
      return AccountProviderId ? true : false;
    }
    // Login strategy is provider
    return true;
  }

  /**
   * Connecting Account to AccountProvider
   * @param {Object} user - User payload from google sign in
   * @param {UserInfo} payload - User payload from google sign in
   */
  static async connectToProvider(user, payload) {
    // id: from Account
    const { id, strategy, roles } = user;
    // For current, user is not connected to provider
    // const modelRole = roles === 'CLIENT' ? Client : Vendor;
    try {
      const data = await sequelize.transaction(async (t) => {
        // Create Account Provider
        const createAccountProvider = await this.create(
          {
            roles: 'CLIENT',
            avatar: payload.photoURL,
            email: payload.email,
          },
          { transaction: t }
        );
        // Update client Account Provider ID
        const updateClient = await Client.update({
          AccountProviderId: createAccountProvider.id,
        }, { where: { AccountId: id }, transaction: t });
        return updateClient;
      })
      return data;
    } catch (error) {
      throw error;
    }
  }
  /**
   * Connecting Account to AccountProvider
   * @param {string} id - Id from Client or Vendor
   * @param {UserInfo} payload - User payload from google sign in
   */
  static async connectToAccount(id, payload) {
    // For current, user is not connected to provider
    // const modelRole = roles === 'CLIENT' ? Client : Vendor;
    try {
      const data = await sequelize.transaction(async (t) => {
        // Create Account Provider
        const createAccountProvider = await this.create(
          {
            roles: 'CLIENT',
            avatar: payload.photoURL,
            email: payload.email,
          },
          { transaction: t }
        );
        // Update client Account Provider ID
        await Client.update({
          AccountProviderId: createAccountProvider.id,
        }, { where: { id }, transaction: t });
        return createAccountProvider.dataValues;
      })
      return data;
    } catch (error) {
      throw error;
    }
  }
}
AccountProvidersService.model = AccountProvider;

module.exports = AccountProvidersService;
