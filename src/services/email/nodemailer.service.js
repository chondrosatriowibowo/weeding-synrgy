const path = require('path');
const nodemailer = require('nodemailer');
const ejs = require('ejs');
const config = require('../../config/nodemailer');

class NodemailerService {
  /**
   * Template HTML body email
   * @param {'verification' | 'forgotPassword'} type - type of email
   * @param {*} props
   * @returns
   */
  static async template(type, props) {
    const views = path.join(__dirname, '..', '..', 'views');
    return await ejs.renderFile(views + '/pages/email/index.ejs', { page: type, props });
  }
  /**
   * Create transporter
   */
  static transporter = nodemailer.createTransport(config);
  /**
   * Send email to user
   * @param {'verification' | 'forgotPassword'} emailType
   * @param {{to: string, subject: string}} info
   * @param {*} props
   * @returns
   */
  static async sendMail(emailType, info, props) {
    const { to, subject } = info;
    // send mail with defined transport object
    const msgBody = this.transporter.sendMail({
      from: `"Promise Team" <${config.auth.user}>`, // sender address
      to, // list of receivers
      subject, // Subject line
      html: await this.template(emailType, props), // html body
    });
    return msgBody;
  }
}

module.exports = NodemailerService;
