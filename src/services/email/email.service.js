const NodemailerService = require('./nodemailer.service');

class EmailService extends NodemailerService {
  /**
   * Send email verification to user
   * @param {'verification' | 'forgotPassword'} emailType
   * @param {{ email: string, verifyCode: string, name: string }} props
   * @returns {Promise<any>}
   */
  static async sendMailVerification(emailType, props) {
    try {
      const { email, verifyCode, name } = props;
      const subject = emailType === 'verification' ? 'Email Verification' : 'Reset password';
      const endpoint = emailType === 'verification' ? 'verify' : 'reset/new';
      // Send email verification
      const info = { to: email, subject: `[Promise] ${subject}` };
      const data = await this.sendMail(emailType, info, {
        url: `https://promised.herokuapp.com:3000/${endpoint}/` + verifyCode,
        name,
      });
      return data;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = EmailService;
