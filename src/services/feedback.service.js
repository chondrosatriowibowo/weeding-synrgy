const { Account, Admin, Client, Vendor, sequelize, Feedback, Product } = require('../models');
const Service = require('./service');
const VendorsService = require('./vendors.service');

class FeedbackService extends Service{
    /**
   * Get All Feedback
   * @returns {Promise<any>} all data
   */
     static async findAllFeedback(offset, limit, params) {
        try {
            return await this.findAllPagination(offset, limit,{
                include: [{
									model: Product,
									attributes: ['name', 'VendorId'],
									where: { VendorId: params }
                },
                {
                  model: Client, 
                  attributes: ['name'],
                  include: [{
                    model: Account,
                    attributes: ['avatar']
                  }]
                } 
              ],
             }) 
        } catch (error) {
            throw error
        }
      }
    /**
   * Get All Feedback
   * @returns {Promise<any>} all data
   */
     static async findVendorRating(params) {
        try {
            function getVendorRating(star){
                return {
									include: 
									[
										{
											model: Product,
											where: {VendorId: params}
                    }
                	],
                where: { rating: star }
              }
            }
            let rating1 = await this.model.count(getVendorRating(1))
            let rating2 = await this.model.count(getVendorRating(2))
            let rating3 = await this.model.count(getVendorRating(3))
            let rating4 = await this.model.count(getVendorRating(4))
            let rating5 = await this.model.count(getVendorRating(5))

            let totalRating = await this.model.count({
              	include: 
                [
                  {
										model: Product,
										where: {VendorId: params}
                  }
								]
							})
            let starOutOf = parseInt(((rating1*1) + (rating2*2) + (rating3*3) + (rating4*4) + (rating5*5))/totalRating)
            let data = 
							{
                allStar: 
									{
                    oneStar : rating1,
                    twoStar : rating2,
                    threeStar : rating3,
                    fourStar : rating4,
                    fiveStar : rating5,
                    totalStar: totalRating,
                    starOutOf : starOutOf
                	},
            		}
            return data
        } catch (error) {
            
        }
     }
    /**
   * Create product
   * @returns {Promise<any>} all data
   */
     static async createFeedback(id, params, payload) {
        try {
            const ClientId = id
            const {productId} = params 
            const data = {
                ...payload,
                ClientId : ClientId,
                ProductId : productId,
            } 
            return await this.model.create(data)        
        } catch (error) {
            throw error
        }
      }
}
FeedbackService.model = Feedback;
  
module.exports = FeedbackService;