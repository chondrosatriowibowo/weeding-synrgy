const { Product, Promo, Feedback, Sequelize, Vendor, sequelize, Account } = require('../models');
const Service = require('./service');
const { Op } = Sequelize;
const { StorageService } = require('./firebase/firebase.service');

class ProductService extends Service {
  /**
   * Find all feedback relations with product
   * @returns {Promise<any>}
   */
  static async findAllFeedback() {
    return await this.model.findAll({
      include: [
        {
          model: Feedback,
          attributes: ['rating, comment, feedbackDate'],
        },
      ],
      raw: true,
    });
  }

  /**
   * Find all product relations with vendor
   * @param {string} id - Vendor ID
   * @returns {Promise<any>}
   */
  static async findProductRelVendor(offset, limit, id) {
    return await this.findAllPagination(offset, limit, {
      attributes: ['id','name', 'price', 'images', 'description', 'capacity'],
      include: [
        {
          model: Vendor,
          attributes: ['id'],
          where: { id },
        },
      ],
      raw: true,
    });
  }

  /**
   * Find one product
   * @returns {Promise<any>}
   */
  static async findOneProduct(id) {
    function getVendorRating(star) {
      return {
        include: [
          {
            model: Feedback,
            where: { rating: star },
          },
        ],
        where: { id },
      };
    }
    let rating1 = await this.model.count(getVendorRating(1));
    let rating2 = await this.model.count(getVendorRating(2));
    let rating3 = await this.model.count(getVendorRating(3));
    let rating4 = await this.model.count(getVendorRating(4));
    let rating5 = await this.model.count(getVendorRating(5));

    let totalRating = await this.model.count({
      include: [
        {
          model: Feedback,
          where: { ProductId: id },
        },
      ],
    });
    let starOutOf = parseInt((rating1 * 1 + rating2 * 2 + rating3 * 3 + rating4 * 4 + rating5 * 5) / totalRating);
    let data = await this.model.findOne({
      where: { id: id },
      attributes: ['images', 'name', 'price', 'capacity', 'description', 'discount'],
      include: [
        {
          model: Vendor,
          attributes: ['id','name', 'banner', 'city', 'categories', 'isVerified', 'phone', 'url'],
          include: [
            {
              model: Account,
              attributes: ['avatar', 'email'],
            },
          ],
        },
      ],
    });
    let result = {
      starOutOf: starOutOf,
      data: data,
    };
    return result;
  }

  /**
   * Find Discount Product
   * @returns {Promise<any>} all data
   */
  static async findDiscountProduct(offset, limit) {
    try {
      return await this.findAllPagination(offset, limit, {
        attributes: ['id', 'name', 'images', 'price', 'discount'],
        where: { discount: { [Op.ne]: 0 } },
        include: [
          {
            model: Vendor,
            attributes: ['name'],
            include: [
              {
                model: Account,
                attributes: ['avatar'],
              },
            ],
          },
        ],
      });
    } catch (error) {
      throw error;
    }
  }

  /**
   * Find All Query Custom Multi Where Options
   * @returns {Promise<any>} all data
   */
  static async findProductByQuery(offset, limit, params) {
    let { categories, sortPrice, search, city, isVerified } = params;
    let queryVendor = {};
    let query = params, q_search;

    delete query.search;
    delete query.offset;
    delete query.limit;
    delete query.categories;
    delete query.city;
    delete query.isVerified;

    if (search) {
      const q_like = { [Op.iLike]: `%${[search]}%` };
      q_search = { [Op.or]: [{ name: q_like }, { description: q_like }] };
    }

    if (categories) {
      // Parse to array if type categories is a string
      if (typeof categories === 'string') {
        categories = [categories];
      }
      // Find matching array
      queryVendor.categories = {
        [Op.contains]: categories,
      };
    }
    if (city) {
      // Find matching city
      queryVendor.city = city;
    }

    if (isVerified) {
      // Find matching isVerified
      queryVendor.isVerified = isVerified;
    }

    if (sortPrice) {
      if (sortPrice === 'min') {
        var order = ['price', 'ASC'];
      } else if (sortPrice === 'max') {
        var order = ['price', 'DESC'];
      }
      delete query.sortPrice;
    } else {
      var order = ['price', 'ASC'];
    }
    return await this.findAllPagination(offset, limit, {
      order: [order],
      attributes: ['id', 'name', 'images', 'price'],
      include: [
        {
          model: Vendor,
          attributes: ['name', 'city'],
          where: { ...queryVendor },
        },
      ],
      where: { ...query, ...q_search },
    });
  }

  /**
   * Create product
   * @returns {Promise<any>} all data
   */
    static async createProduct(id, files, payload) {
      try {
          //Variable to hold the request
          const VendorId = id
          const {images} = files

          //Loop images file to generate all url & append it into array
          let imagesAllURL = []
          for(let image in images){
              var imageURL = await StorageService.upload('products', images[image], 'image')
              imagesAllURL.push(imageURL)
          }
          //Check if there aren't images uploaded throw error
          if(imagesAllURL.length === 0){
            throw new Error('error')
          }
          //Check if there are unposted file but image already uploaded, delete the image in the firebase then.
          if(!payload.name || !payload.description || !payload.price){
            if(imagesAllURL){
              await StorageService.delete(imagesAllURL)
            }
            throw new Error('error')
          }
          let updatedData =
          {
            ...payload,
            VendorId: VendorId,
            images: imagesAllURL,
          }
        return await this.model.create(updatedData)
      } catch (error) {
        throw error
      }
    }


  /**
   * Update product
   * @returns {Promise<any>} all data
   */
  static async updateProduct(params, files, payload) {
    try {
      //Variable to hold the request
      const { id } = params;
      let { images } = await this.model.findOne({ where: { id: id } });

      var sendedImages = await files.images;

      //If user update the image and image slot is not empty update it to the new one
      if (payload.images_user) {
        var selectedImages = JSON.parse(payload.images_user);
        for (let image in images) {
          for (let select in selectedImages) {
            if (selectedImages[select] === images[image]) {
              var imagesURL = await StorageService.upload('products', sendedImages[select], 'image');
              await StorageService.delete(selectedImages[select]);
              images = images.filter((item) => item != images[image]);
              images.push(imagesURL);
            }
          }
        }
      }
      //If user update the image and image slot is empty add and update it
      if (!selectedImages && sendedImages) {
        var imagesURL = await StorageService.upload('products', sendedImages[0], 'image');
        images.push(imagesURL);
      }
      let data = {
        ...payload,
        images: images,
      };
      return await this.model.update(data, { where: { id } });
    } catch (error) {
      throw error;
    }
  }

  /**
   * Find prodcut cart
   * @param {Array<{id: string, quantity: number}>} items - items from Checkout
   */
  static async findProductCart(items) {
    // Get product ID property
    const productId = items.map((obj) => obj.id);
    const data = await Vendor.findAll({
      attributes: ['id', 'name', 'categories', 'isVerified'],
      include: [
        {
          model: this.model,
          attributes: ['id', 'name', 'price', 'images', 'discount'],
          where: { id: { [Op.in]: productId } },
        },
      ],
    });
    const getElementLocal = (id, el) => items.find((obj) => obj.id === id)[el];

    for (const el of data) {
      for (const prod of el.Products) {
        prod.dataValues.quantity = getElementLocal(prod.dataValues.id, 'quantity');
        prod.dataValues.book_date = getElementLocal(prod.dataValues.id, 'book_date');
      }
    }
    return data;
  }
}

ProductService.model = Product;

module.exports = ProductService;
