const { Order, Product, Client, Vendor, OrderProduct, Sequelize, sequelize } = require('../models');
const { Op } = Sequelize;
const Service = require('./service');

class OrdersService extends Service {
  /**
   * Include multiple Models
   */
  static multiIncludes = [
    {
      model: Client,
    },
    {
      model: Product,
      include: [
        {
          model: Vendor,
          attributes: ['name', 'categories'],
        },
      ],
      through: {
        attributes: ['price', 'progress', 'quantity'],
      },
    },
  ];
  /**
   * Find all order relations with product
   * @returns {Promise<any>}
   */
  static async findAllRelProduct() {
    return await this.model.findAll({
      include: this.multiIncludes,
      // raw: true,
    });
  }

  /**
   * Find all list OrderProduct by Order ID
   * @param {string} OrderId - Order ID
   * @returns {Promise<any>}
   */
  static async findOneRelProduct(OrderId) {
    return await this.model.findOne({
      include: this.multiIncludes,
      where: { id: OrderId },
      // raw: true,
    });
  }

  /**
   * Get checkout payload for payment
   * @param {Array<{id: string, name: string, book_date: string, price: number, discount: number, sub_total: number, quantity: number}>} items
   * @returns {Promise<{gross_amount: number, item_details: Array<{id: string, name: string, price: number, quantity: number}>}>}
   */
  static async getCheckoutPayload(items) {
    // Sum total
    const gross_amount = items.reduce((product, { sub_total }) => product + sub_total, 0);
    // Remove sub total & discount property
    for (const el of items) {
      delete el.sub_total;
      delete el.discount;
      delete el.book_date;
    }
    // Return payload token transaction
    return { gross_amount, item_details: items };
  }

  /**
   * Create Order
   * @param {{payment_info: Object,
   * item_details: Array<{id: string, name: string, book_date: string, price: number, quantity: number}>}} payload
   * @param {string} ClientId - Payload from cookies
   * @returns {Promise<any>} inserted data
   */
  static async createTransactionOrder(payload, ClientId) {
    try {
      const data = await sequelize.transaction(async (t) => {
        const { item_details, payment_info } = payload;
        const { gross_amount, transaction_time, transaction_status, payment_type } = payment_info;
        // Create order
        const createOrder = await Order.create(
          {
            id: payment_info.order_id,
            ClientId,
            total: gross_amount,
            date: new Date(transaction_time),
            status: transaction_status,
            payment_type,
          },
          { transaction: t }
        );
        // Set OrderId and added key ProductId based on id
        item_details.forEach((el) => {
          el.OrderId = payment_info.order_id;
          el.ProductId = el.id;
        });
        // Bulk create OrderProduct
        await OrderProduct.bulkCreate(item_details, {
          validate: true,
          transaction: t,
        });

        return { ...createOrder.dataValues, item_details };
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Update progress status in OrderProduct
   * @param {string} OrderId - Order ID
   * @param {string} ProductId - Product ID
   * @param {string} progress - Progress
   * @param {'DEFAULT' | 'VENDOR'} type - Request type
   * @returns {Promise<any>}
   */
  static async updateProgress(OrderId, ProductId, progress, type = 'DEFAULT') {
    try {
      // Vendor needs to pass ProductId
      if (type === 'VENDOR' && ProductId == undefined) throw new Error('ProductId is required');
      // Where options for OrderProduct
      const whereOptions = ProductId === undefined ? { OrderId } : { [Op.and]: [{ OrderId }, { ProductId }] };
      // Get current progress status from OrderProduct
      const status = await OrderProduct.findOne({ where: whereOptions });
      // Vendor can't update progress status to UNPAID and CANCEL manually
      if (progress === 'UNPAID' || progress === 'CANCEL' || progress === 'CONFIRMATION') {
        throw new Error("Vendor can't update progress status manually to UNPAID, CONFIRMATION, CANCEL");
      }
      // If current progress is UNPAID (user haven't pay the order)
      if (status.progress === 'UNPAID') {
        if (progress === 'REFUND') {
          throw new Error("Vendor can't update progress to REFUND, if user haven't pay yet");
        } else if (progress === 'PREPARATION' || progress === 'FINISH') {
          throw new Error("Vendor can't update progress to FINISH or PREPARATION, if user haven't pay yet");
        }
      } else if (status.progress === 'REFUND') {
        if (progress) {
          throw new Error('Order have been refunded, you can not update progress status');
        }
      }
      // Convert status from transaction to OrderProduct status
      if (progress === 'settlement' || progress === 'capture' || progress === 'challenge') {
        progress = 'CONFIRMATION';
      } else if (progress === 'failure') {
        progress = 'CANCEL';
      } else if (progress === 'refund') {
        progress = 'REFUND';
      }

      const update = await OrderProduct.update({ progress }, { where: whereOptions });
      return update;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Find All OrderProduct by User ID
   * @param {string} id - User ID
   * @param {Object} params - Query params
   */
  static async findAllOrderProductByUserId(id, params) {
    try {
      const { isSession, ClientId } = params;
      const query = params;
      if (isSession || ClientId) {
        delete query.isSession;
        delete query.ClientId;
      }

      const data = await OrderProduct.findAll({
        include: [
          {
            model: Order,
            where: { ClientId: id },
          },
          {
            model: Product,
            include: [
              {
                model: Vendor,
                attributes: ['id', 'name', 'isVerified', 'banner', 'categories']
              }
            ]
          },
        ],
        where: query,
      });
      return data;
    } catch (error) {
      return [];
    }
  }
}
OrdersService.model = Order;

module.exports = OrdersService;
